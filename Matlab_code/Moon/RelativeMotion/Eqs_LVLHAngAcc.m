function omegad_lm_L = Eqs_LVLHAngAcc(r_st, rd_st_M, rdd_st_M, rddd_st_M)

    omega_lm = Eqs_LVLHAngVel(r_st, rd_st_M, rdd_st_M);
    
    h = cross(r_st, rd_st_M);
    hd = cross(r_st, rdd_st_M);
    %hdd = cross(rd_st_M, rdd_st_M) + cross(r_st,rddd_st_M);
    
    r_normd = dot(r_st,rd_st_M)/norm(r_st);
    h_normd = dot(h,hd)/norm(h);

    omegad_lm_y = -(h_normd/norm(r_st) + 2*r_normd*omega_lm(2))/norm(r_st);
    
    omegad_lm_z = ...
        omega_lm(3)*(r_normd/norm(r_st) - 2*h_normd/norm(h)) ...
            + norm(r_st)*dot(-h, rddd_st_M)/norm(h)^2;
%         + norm(r_st)*dot(rd_st_M, cross(r_st, rddd_st_M))/norm(h)^2;
       
    omegad_lm_L = [0; omegad_lm_y; omegad_lm_z];

end

% Vecchia funzione
% function omegad_lm_L = Eqs_LVLHAngAcc(r_st, rd_st_M, rdd_st_M, rddd_st_M)
% 
%     rnormd = r_st'*rd_st_M/norm(r_st);
%     hnorm = norm(cross(r_st, rd_st_M));
%     
%     omegad_lm_y = ...
%         - ( rdd_st_M(1) - 2 * rnormd*rd_st_M(1)/norm(r_st) )/norm(r_st);
%     
% %casotto
% %     omegad_lm_y = ...
% %         norm(r_mt)^-1 * (rdd_mt_M(1) - 2 * rnormd*rd_mt_M(1)/norm(r_mt));
%     
%     omegad_lm_z = ...
%         (norm(r_st)/hnorm)*( ...
%             rnormd*rdd_st_M(2)/norm(r_st) ...
%             + ...
%             rddd_st_M(2) ...
%             - ...
%             2*norm(r_st)/hnorm*rdd_st_M(1)*rdd_st_M(2) ...
%         );
% 
%     omegad_lm_L = [0; omegad_lm_y; omegad_lm_z];
%     
% end