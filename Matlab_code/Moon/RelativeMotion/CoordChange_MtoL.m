function C_MtoL = CoordChange_MtoL(r_st, rd_st_M)

    % r_st ad rd_st_M expressed in M!!!  
    
    j = - cross(r_st, rd_st_M)/norm(cross(r_st, rd_st_M));
    k = - r_st/norm(r_st);
    i = cross(j,k);
    
    C_MtoL = zeros(3,3);
    
    C_MtoL(:,1) = i;
    C_MtoL(:,2) = j;
    C_MtoL(:,3) = k;
    
    C_MtoL = C_MtoL';
end

