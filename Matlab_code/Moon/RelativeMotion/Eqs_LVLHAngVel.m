function omega_lm = Eqs_LVLHAngVel(r_st, rd_st_M, rdd_st_M)
    
    % tutto espresso in LVLH
    
    h = cross(r_st, rd_st_M);
    hd = cross(r_st, rdd_st_M);
    
    omega_lm = [ ...
        0; ...
        - norm(h)/norm(r_st)^2; ...
        norm(r_st)*dot(rd_st_M, hd)/norm(h)^2;
    ];

end

% Vecchia funzione
% function omega_lm = RelativeMotion_AngVel(r_st, rd_st_M, rdd_st_M)
%     
%     % tutto espresso in LVLH
%     
%     omega_lm = [ ...
%         0; ...
%         - rd_st_M(1)/norm(r_st); ...
%         norm(r_st)*rdd_st_M(2)/norm(cross(r_st,rd_st_M)); ...
%     ];
% 
% end