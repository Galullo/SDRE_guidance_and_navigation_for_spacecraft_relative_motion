function rdd = Eqs_ER3BP(r, rd, r_ps, omega, omegad, mu)
    
    rdd = - 2 * cross(omega, rd) ...
          - cross(omegad, r) ...
          - cross(omega, cross(omega, r)) ...
          - mu * r/norm(r)^3 ...
          - (1 - mu) * ((r_ps + r)/norm(r_ps + r)^3 - r_ps/norm(r_ps)^3);
      
end

