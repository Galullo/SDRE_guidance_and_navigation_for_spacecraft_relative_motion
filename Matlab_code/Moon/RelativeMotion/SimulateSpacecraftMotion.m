function SpacecraftTS = SimulateSpacecraftMotion(r0, rd_M0, SecondaryTS, EqsType, TimeSpan, R3BPar, OdeOptions)
    
    % ---------------------------------------------------------------------
    % Coordinate frames
    % ---------------------------------------------------------------------
    % P : primary-centered perifocal frame
    % M : secondary-centered rotating frame
    % ---------------------------------------------------------------------
    
    % ---------------------------------------------------------------------
    % Input arguments
    % ---------------------------------------------------------------------
    % r0        : initial s/c position in M
    % rd_M0     : initial s/c velocity in M
    % EqsType   : eqs. set for simulation ('CR3BP' or 'ER3BP')
    % SecondaryTS
    %   - r        : secondary position wrt primary in M (timeseries)
    %   - omega    : secondary angular velocity wrt primary in M 
    %                (timeseries)
    %   - omegad_P : secondary angular acceleration wrt primary in primary
    %                as seen from P, expressed in M (timeseries)
    % ---------------------------------------------------------------------
    
    % ---------------------------------------------------------------------
    % Output arguments
    % ---------------------------------------------------------------------
    % SpacecraftTS
    %   - r       : s/c position wrt secondary in M (timeseries)
    %   - rd_M    : s/c velocity wrt secondary as seen from M, expressed 
    %               in M (timeseries)
    %   - rdd_M   : s/c acceleration wrt secondary as seen from M, 
    %               expressed in M (timeseries)
    %   - EqsType : eqs. set used for simulation
    % ---------------------------------------------------------------------
    
    if strcmp(EqsType, 'CR3BP')
        [t, State] = ode45(@(t, State)SpacecraftMotion_CR3BP(t, State, R3BPar.mu), TimeSpan, [r0; rd_M0], OdeOptions);
    elseif strcmp(EqsType, 'ER3BP')
        [t, State] = ode45(@(t, State)SpacecraftMotion_ER3BP(t, State, SecondaryTS.r, SecondaryTS.omega, SecondaryTS.omegad_P, R3BPar.mu), TimeSpan, [r0; rd_M0], OdeOptions);
    elseif strcmp(EqsType, 'KeplerianSecondary')
        [t, State] = ode45(@(t, State)SpacecraftMotion_KeplerianSecondary(t, State, R3BPar.mu), TimeSpan, [r0; rd_M0], OdeOptions);
    else
        error('ERROR : EqsType must be CR3BP, ER3BP, or KeplerianSecondary')
    end
    
    r = State(:,1:3);
    rd_M = State(:,4:6);
    
    rdd_M = zeros(length(t),3);
    
    for k = 1 : length(t)
        
        if strcmp(EqsType, 'CR3BP')
            State = SpacecraftMotion_CR3BP(t(k), [r(k,:)'; rd_M(k,:)'], R3BPar.mu);
        elseif strcmp(EqsType, 'ER3BP')
            State = SpacecraftMotion_ER3BP(t(k), [r(k,:)'; rd_M(k,:)'], SecondaryTS.r, SecondaryTS.omega, SecondaryTS.omegad_P, R3BPar.mu);
        else
            State = SpacecraftMotion_KeplerianSecondary(t(k), [r(k,:)'; rd_M(k,:)'], R3BPar.mu);
        end
        
        rdd_M(k,:) = State(4:6,:)';
        
    end
       
    SpacecraftTS.r = timeseries(r, t, 'Name', 'S/C position in M');
    SpacecraftTS.r.DataInfo.Units = 'a';
    SpacecraftTS.r.TimeInfo.Units = 'n';
    
    SpacecraftTS.rd_M = timeseries(rd_M, t, 'Name', 'S/C velocity in M');
    SpacecraftTS.rd_M.DataInfo.Units = 'a';
    SpacecraftTS.rd_M.TimeInfo.Units = 'n';
    
    SpacecraftTS.rdd_M = timeseries(rdd_M, t, 'Name', 'S/C acceleration in M');
    SpacecraftTS.rdd_M.DataInfo.Units = 'a';
    SpacecraftTS.rdd_M.TimeInfo.Units = 'n';
    
    SpacecraftTS.EqsType = EqsType;
   
end

function dState = SpacecraftMotion_ER3BP(t, State, r_psTS, omegaTS, omegadTS, mu)

    % ODE Function: ER3BP equations expressed in M
    
    % Input arguments
    % ---------------------------------------------------------------------
    % omegaTS  : timeseries in primary P
    % omegadTS : timeseries in primary P
    % r_psTS   : timeseries in P
    % mu       : normalized gravitational parameter
    % ---------------------------------------------------------------------
    
    r = State(1:3);
    rd = State(4:6);
    
    % r_ps from P to M
    r_ps = [-norm(interp1(r_psTS.Time, r_psTS.Data, t)); 0; 0];
    
    % omegaTS e omegadTS same in P and in M
    omega = interp1(omegaTS.Time, omegaTS.Data, t)';
    omegad = interp1(omegadTS.Time, omegadTS.Data, t)';
    
    % Compute acceleration according to ER3BP
    rdd = Eqs_ER3BP(r, rd, r_ps, omega, omegad, mu);
    
    dState = [rd; rdd];

end

function dState = SpacecraftMotion_CR3BP(t, State, mu)

    % ODE Function: ER3BP equations expressed in M
    
    % Input arguments
    % ---------------------------------------------------------------------
    % mu : normalized gravitational parameter
    % ---------------------------------------------------------------------
    
    r = State(1:3);
    rd = State(4:6);
    
    % r_ps, omega, and omegad for CR3BP in normalized units
    r_ps = [-1; 0; 0];
    omega = [0; 0; 1];
    
    % Compute acceleration according to CR3BP
    rdd = Eqs_CR3BP(r, rd, omega, r_ps, mu);
    
    dState = [rd; rdd];

end

function dState = SpacecraftMotion_KeplerianSecondary(t, State, mu)

    % ODE Function: ER3BP equations expressed in M
    
    % Input arguments
    % ---------------------------------------------------------------------
    % mu : normalized gravitational parameter
    % ---------------------------------------------------------------------
    
    r = State(1:3);
    rd = State(4:6);
    
    % Compute acceleration according to CR3BP
    rdd = - mu*r/norm(r)^3;
    
    dState = [rd; rdd];

end



