function RelMotionTS = RelativeMotionEqs(TargetTS, SecondaryTS, T, R3BPPar, OdeOptions)
    
    % Input parameters
    % ---------------------------------------------------------------------
    % TargetTS
    %   - r_st     : target position wrt secondary in M 
    %   - rd_st_M  : target velocity wrt secondary in M
    %   - rdd_mt_M : target acceleration wrt secondary in M 
    % SecondaryTS
    %   - r            : secondary position wrt primary in P
    %   - rd_em_P      : secondary velocity wrt primary in P 
    %   - omega_sp     : secondary angular velocity wrt primary in P
    %   - omegad_ps_P  : secondary angular acceleration wrt primary in P
    %   - omegadd_ps_P : secondary angular acceleration derivative wrt 
    %                    primary in P
    % T          : simulation time
    % R3BPPar    : restricted three-body problem parameters
    % OdeOptions : 
    % ---------------------------------------------------------------------
    
    % Output parameters
    % ---------------------------------------------------------------------
    % RelMotionTS
    %   - rho         :
    %   - rhod_L      :
    %   - omega_ls    :
    %   - omegad_ls_L :
    % ---------------------------------------------------------------------
     
    [t, State] = ode45(...
        @(t, State)SpacecraftMotion_ER3BP( ...
            t, State, ...
            TargetTS.r, TargetTS.rd_M, TargetTS.rdd_M, ...
            SecondaryTS.r, SecondaryTS.rd_P, SecondaryTS.omega, SecondaryTS.omegad_P, SecondaryTS.omegadd_P, ...
            R3BPar.mu));
    
    r = State(:,1:3);
    rd_M = State(:,4:6);
    
    rdd_M = zeros(length(t),3);
    
    for k = 1 : length(t)
        
        State = SpacecraftMotion_ER3BP(t(k), [r(k,:)'; rd_M(k,:)'], r_psTS, omega_spTS, omegad_spTS, R3BPar.mu);
        
        rdd_M(k,:) = State(4:6,:)';
        
    end
       
    
    rho, rhod_L, omega_ls, omegad_ls_L
    r = timeseries(r, t, 'Name', 'Spacecraft pos. in M');
    r.DataInfo.Units = 'a';
    r.TimeInfo.Units = 'n';
    
    rd_M = timeseries(rd_M, t, 'Name', 'Spacecraft vel. in M');
    rd_M.DataInfo.Units = 'a';
    rd_M.TimeInfo.Units = 'n';
    
    rdd_M = timeseries(rdd_M, t, 'Name', 'Spacecraft acc. in M');
    rdd_M.DataInfo.Units = 'a';
    rdd_M.TimeInfo.Units = 'n';
   
end


function dState = RelativeMotion(t, State, ...
        r_stTS, rd_st_MTS, rdd_st_MTS, ...
        r_psTS, rd_ps_PTS, omega_spTS, omegad_sp_PTS, omegadd_sp_PTS, ...
        mu)
         
        % r_stTS : in M
        % rd_stTS : in M
        % rdd_stTS : in M
        
        % r_psTS : in P
        % rd_ps_P : in P
        % omega_ps : in P 
        % omegad_ps_P: in P
        % omegadd_ps_P : in P
        
        rho = State(1:3);
        rhod_L = State(4:6);
        
        % Compute coordinate change matrices
        C_PtoM = CoordChange_PtoM();
        C_MtoL = CoordChange_MtoL();
        C_PtoL = C_MtoL*C_PtoM;
        
        % Target variables: extraction and conversion from M to LVLH
        r_st = C_MtoL*interp1(r_stTS.Time, r_stTS.Data, t)';
        rd_st_M = C_MtoL*interp1(rd_st_MTS.Time, rd_st_MTS.Data, t)';
        rdd_st_M = C_MtoL*interp1(rdd_st_MTS.Time, rdd_st_MTS.Data, t)';
        
        % Secondary variables: extraction and conversion from P to LVL
        % Note that omegad_sp_P == omegad_sp_M due to frames definitions,
        % and the same holds for omegadd_sp_P
        r_ps = C_PtoL*interp1(r_psTS.Time, r_psTS.Data, t)';
        rd_ps_P = C_PtoL*interp1(rd_ps_PTS.Time, rd_ps_PTS.Data, t)';
        omega_sp = C_PtoL*interp1(omega_spTS.Time, omega_spTS.Data, t)';
        omegad_sp_M = C_PtoL*interp1(omegad_sp_PTS.Time, omegad_sp_PTS.Data, t)';
        omegadd_sp_M = C_PtoL*interp1(omegadd_sp_PTS.Time, omegadd_sp_PTS.Data, t)';
        
        % Compute target jerk wrt secondary
        rddd_st_M = RelativeMotion_Jerk(...
            r_st, rd_st_M, rdd_st_M, ...
            r_ps, rd_ps_P, omega_sp, omegad_sp_M, omegadd_sp_M, ...
            mu);
        
        % Compute LVLH angular velocity and acceleration wrt secondary
        omega_lm = RelativeMotion_AngVel(r_st, rd_st_M, rdd_st_M);
        omegad_lm_L = RelativeMotion_AngAcc(r_st, rd_st_M, rdd_st_M, rddd_st_M);
        
        % Compute LVLH angular velocity and acceleration wrt primary
        omega_lp = omega_lm + omega_sp;
        omegad_lp_L = omegad_lm_L + omegad_sp_M - cross(omega_lm, omega_sp);
        
        % Compute relative acceleration
        rhodd_L = ...
            - 2 * cross(omega_lp, rhod_L) ...
            - cross(omegad_lp_L, rho) ...
            - cross(omega_lp, cross(omega_lp, rho)) ...
            + mu * (r_st/norm(r_st)^3 - (r_st + rho)/norm(r_st + rho)^3) ...
            + (1-mu) * ((r_ps + r_st)/norm(r_ps + r_st)^3 - (r_ps + r_st + rho)/norm(r_ps + r_st + rho)^3);
        
        
        dState = [rhod_L; rhodd_L];
        
        
end
