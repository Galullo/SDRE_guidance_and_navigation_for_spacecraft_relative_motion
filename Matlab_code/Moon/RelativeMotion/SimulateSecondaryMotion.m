function SecondaryTS = SimulateSecondaryMotion(f0, TimeSpan, R3BPar, OdeOptions)
    
    % ---------------------------------------------------------------------
    % Coordinate frames
    % ---------------------------------------------------------------------
    % P : primary-centered perifocal frame
    % M : secondary-centered rotating frame
    % ---------------------------------------------------------------------
    
    % ---------------------------------------------------------------------
    % Input arguments
    % ---------------------------------------------------------------------
    % f0     : initial secondary true anomaly [rad]
    % T      : simulation time
    % R3BPar : restricted three-body problem parameters
    % ---------------------------------------------------------------------
    
    % ---------------------------------------------------------------------
    % Output arguments
    % ---------------------------------------------------------------------
    % SecondaryTS
    %   - r         : secondary position wrt primary in M (timeseries)
    %   - rd_P      : secondary velocity wrt primary as seen in P, 
    %                 expressed in M (timeseries)
    %   - rdd_P     : secondary acceleration wrt primary as seen in P, 
    %                 expressed in M (timeseries)
    %   - f         : true anomaly (timeseries)
    %   - omega     : secondary angular vel. wrt primary in M (timeseries)
    %   - omegad_P  : secondary angular acc. wrt primary as seen in M/P, 
    %                 expressed in M (timeseries)
    %   - omegadd_P : secondary angular acc. derivative wrt primary as seen 
    %                 in M/P, expressed in M (timeseries)
    % ---------------------------------------------------------------------
    
    % Initial secondary position, and velocity from true anomaly in P.
    r0 = R3BPar.p/(1 + R3BPar.e*cos(f0)) * [cos(f0); sin(f0); 0];     
    rd0 = sqrt((1-R3BPar.mu)/R3BPar.p)*[-sin(f0); R3BPar.e + cos(f0); 0];
    
    % Simulate secondary motion
    [t,State] = ode45(@(t,State)KeplerianMotion(t,State, R3BPar.mu), TimeSpan, [r0; rd0; f0], OdeOptions);
    
    r    = State(:,1:3);
    rd_P = State(:,4:6);
    f    = State(:,7);
    
    rdd_P     = zeros(length(t),3);
    omega     = zeros(length(t),3);
    omegad_P  = zeros(length(t),3);
    omegadd_P = zeros(length(t),3);
    
    for k = 1 : length(t)
        
        r_norm   = norm(r(k,:));
        r_normd  = NormDer(r(k,:), rd_P(k,:), 0, 1);
        
        rdd_P(k,:) = - (1-R3BPar.mu)*r(k,:)/r_norm^3;
        
        r_normdd = NormDer(r(k,:), rd_P(k,:), rdd_P(k,:), 2);
        
        h_norm = norm(cross(r(k,:), rd_P(k,:)));
        
        % omega, omegad_P, and omegadd_P are the same in M and in P
        omega(k,3)     = h_norm/r_norm^2;
        omegad_P(k,3)  = -2*h_norm*r_normd/r_norm^3;
        omegadd_P(k,3) = -2*h_norm*(r_normdd - 3*r_normd^2/r_norm)/r_norm^3;
        
        % Coordinate change matrix C:P->M
        C_PtoM = angle2dcm(pi + f(k), 0, 0, 'ZYX');
        
        % Convert r, rd_P, and rdd_P in M
        r(k,:)     = (C_PtoM * r(k,:)')';
        rd_P(k,:)  = (C_PtoM * rd_P(k,:)')';
        rdd_P(k,:) = (C_PtoM * rdd_P(k,:)')';
        
    end
       
    SecondaryTS.r = timeseries(r, t, 'Name', 'Secondary pos. in M');
    SecondaryTS.r.DataInfo.Units = 'a';
    SecondaryTS.r.TimeInfo.Units = 'n';
    
    SecondaryTS.rd_P = timeseries(rd_P, t, 'Name', 'Secondary vel. in M');
    SecondaryTS.rd_P.DataInfo.Units = 'a';
    SecondaryTS.rd_P.TimeInfo.Units = 'n';
    
    SecondaryTS.rdd_P = timeseries(rdd_P, t, 'Name', 'Secondary acc. in M');
    SecondaryTS.rdd_P.DataInfo.Units = 'a';
    SecondaryTS.rdd_P.TimeInfo.Units = 'n';
    
    SecondaryTS.f = timeseries(f, t, 'Name', 'Secondary true anomaly');
    SecondaryTS.f.DataInfo.Units = 'rad';
    SecondaryTS.f.TimeInfo.Units = 'n';
    
    SecondaryTS.omega = timeseries(omega, t, 'Name', 'Secondary angular vel. in M');
    SecondaryTS.omega.DataInfo.Units = 'a';
    SecondaryTS.omega.TimeInfo.Units = 'n';
    
    SecondaryTS.omegad_P = timeseries(omegad_P, t, 'Name', 'Secondary angular acc. in M');
    SecondaryTS.omegad_P.DataInfo.Units = 'a';
    SecondaryTS.omegad_P.TimeInfo.Units = 'n';
    
    SecondaryTS.omegadd_P = timeseries(omegadd_P, t, 'Name', 'Secondary angular acc. der. in M');
    SecondaryTS.omegadd_P.DataInfo.Units = 'a';
    SecondaryTS.omegadd_P.TimeInfo.Units = 'n';
    
end

function dState = KeplerianMotion(t, State, mu)

    r = State(1:3);
    rd = State(4:6);
    
    rdd = - (1-mu)*r/norm(r)^3;
    omega = norm(cross(r,rd))/norm(r)^2;
    
    dState = [rd; rdd; omega];

end