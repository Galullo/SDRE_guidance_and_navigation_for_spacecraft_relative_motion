function J = GravJacobian(x)
    J = (eye(3) - 3*(x*x')/norm(x)^2)/norm(x)^3;
end