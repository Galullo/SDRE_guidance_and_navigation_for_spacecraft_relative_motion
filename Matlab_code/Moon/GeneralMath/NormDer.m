function dNorm = NormDer(v, vd, vdd, order)

    if order == 1
        dNorm = dot(v,vd)/norm(v);
    elseif order == 2
        dNorm = (norm(vd)^2 + dot(v,vdd) - (dot(v,vd)/norm(v))^2)/norm(v);
    else
        error('ERROR NormDer : order must be equal to 1 or 2');
    end
    
end

