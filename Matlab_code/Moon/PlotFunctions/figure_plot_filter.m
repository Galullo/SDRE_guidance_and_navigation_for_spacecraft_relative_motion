% figure plot for filter

data_editing

%% Figure parameters
TickSize = 12;
LabelSize = 18;
LineSize = 2;
LegendSize = 16;

%%
figure, title('Positions')
hold on, grid on
plot(time_nn(1:end-1), rho_est_nn(:,1),'b', 'LineWidth', LineSize), 
plot(time_nn(1:end-1), rho_est_nn(:,2),'r', 'LineWidth', LineSize), 
plot(time_nn(1:end-1), rho_est_nn(:,3),'k', 'LineWidth', LineSize),
plot(time_nn, rho_ER3BP_nn(:,1),'b--', 'LineWidth', LineSize), 
plot(time_nn, rho_ER3BP_nn(:,2),'r--', 'LineWidth', LineSize), 
plot(time_nn, rho_ER3BP_nn(:,3),'k--', 'LineWidth', LineSize),
legend('x_e','y_e','z_e','x','y','z'), 
xlabel('time [min]'), ylabel('km')


figure, title('Velocities')
hold on, grid on
plot(time_nn(1:end-1), rhod_est_nn(:,1),'b', 'LineWidth', LineSize), 
plot(time_nn(1:end-1), rhod_est_nn(:,2),'r', 'LineWidth', LineSize), 
plot(time_nn(1:end-1), rhod_est_nn(:,3),'k', 'LineWidth', LineSize),
plot(time_nn, rhod_ER3BP_nn(:,1),'b--', 'LineWidth', LineSize), 
plot(time_nn, rhod_ER3BP_nn(:,2),'r--', 'LineWidth', LineSize), 
plot(time_nn, rhod_ER3BP_nn(:,3),'k--','LineWidth', LineSize),
legend('v_{xe}','v_{ye}','v_{ze}','v_x','v_y','v_z'), 
xlabel('time [min]'), ylabel('m/s')
