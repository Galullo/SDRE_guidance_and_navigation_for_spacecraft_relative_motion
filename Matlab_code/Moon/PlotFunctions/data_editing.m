%% Data editing
% time in min
time_nn = INVNORM_TIME(time)./min;              % min

% relative position and velocity
rho_ER3BP_nn = INVNORM_POS(rho_sim);            % [km]
rhod_ER3BP_nn = INVNORM_VEL(rhod_sim)*1e3;      % [m/s]

% % estimated
% rho_est_nn = INVNORM_POS(X_estimated(:,1:3));
% rhod_est_nn = INVNORM_VEL(X_estimated(:,4:6))*1e3;

% chaser and target position in Moon Frame
rc_ER3BP_nn = INVNORM_POS(rc_sim);              % [km]
rt_ER3BP_nn = INVNORM_POS(rt_sim);              % [km]

% Control law
u_L_nn = INVNORM_ACC(u_L)*1e3;                  % [m/s^2]
