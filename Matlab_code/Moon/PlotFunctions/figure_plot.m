%% Load Data
data_editing;

%% Figure parameters
TickSize = 12;
LabelSize = 18;
LineSize = 3;
LegendSize = 16;

%% relative motion in LVLH frame
figure, title('\textbf{Relative motion in LVLH}','Interpreter','latex', 'FontSize', LabelSize),
hold on, grid on, box on, %axis equal

line(-20*[0 Cz(1,4)],20*[0, Cz(1,6)],'Color','k', 'LineStyle', '--', 'LineWidth', 2)
line(20*[0 Cz(2,4)],-20*[0, Cz(2,6)],'Color','k', 'LineStyle', '--', 'LineWidth', 2)
plot3(rho_nn(1,1), rho_nn(1,3),rho_nn(1,2),'ro','LineWidth', LineSize),
plot3(rho_nn(:,1), rho_nn(:,3),rho_nn(:,2),'r','LineWidth', LineSize),
plot3(0 ,0 , 0, 'b*','LineWidth',LineSize), 

xlim([-25 5]), ylim([-15 15])
xlabel('V-bar [km]','Interpreter','latex', 'FontSize', LabelSize,'Position',[-20, 0 ,0]),
ylabel('R-bar [km]', 'Interpreter','latex', 'FontSize', LabelSize), 
zlabel('H-bar [km]', 'Interpreter','latex', 'FontSize', LabelSize),
set(gca,'TickLabelInterpreter', 'latex','FontSize',TickSize,...
    'XAxisLocation', 'origin', 'YAxisLocation', 'origin')

%% Plot motion in Moon frame
figure,  title('\textbf{Relative motion in Moon frame}', 'Interpreter','latex', 'FontSize', LabelSize)
hold on, grid on, axis equal

plot3(NROL1South_X, NROL1South_Y, NROL1South_Z,'k', 'LineWidth', 1)
plot3(rc_nn(1,1), rc_nn(1,2),rc_nn(1,3),'ro','LineWidth',LineSize), 
plot3(rc_nn(:,1), rc_nn(:,2),rc_nn(:,3),'r','LineWidth',LineSize),
plot3(rt_nn(1,1), rt_nn(1,2),rt_nn(1,3),'bo','LineWidth',LineSize),
plot3(rt_nn(:,1), rt_nn(:,2),rt_nn(:,3),'b--','LineWidth',LineSize),
plot3(0 ,0 , 0, 'g*','LineWidth',LineSize),

xlabel('x [km]','Interpreter','latex', 'FontSize', LabelSize),
ylabel('y [km]', 'Interpreter','latex', 'FontSize', LabelSize), 
zlabel('z [km]', 'Interpreter','latex', 'FontSize', LabelSize),
set(gca,'TickLabelInterpreter', 'latex','FontSize',TickSize)


%% Relative Velocities
figure, title('\textbf{Relative Velocities}', 'Interpreter','latex', 'FontSize', LabelSize)
hold on, grid on,

plot(time_nn,rhod_nn(:,1),'r','LineWidth',LineSize),
plot(time_nn,rhod_nn(:,2),'b','LineWidth',LineSize),
plot(time_nn,rhod_nn(:,3),'g','LineWidth',LineSize),

xlabel('time [min]','Interpreter','latex', 'FontSize', LabelSize),
ylabel('v [m/s]', 'Interpreter','latex', 'FontSize', LabelSize), 

legend({'v_x','v_y','v_z'},'Location','NE')
set(gca,'TickLabelInterpreter', 'latex','FontSize',TickSize)

%% Control Accelerations
figure, title('\textbf{Control}', 'Interpreter','latex', 'FontSize', LabelSize)
hold on, grid on,

plot(time_nn,u_L_nn(:,1),'r','LineWidth',LineSize),
plot(time_nn,u_L_nn(:,2),'b','LineWidth',LineSize),
plot(time_nn,u_L_nn(:,3),'g','LineWidth',LineSize),

xlabel('time [min]','Interpreter','latex', 'FontSize', LabelSize),
ylabel('u $[m/s^2]$', 'Interpreter','latex', 'FontSize', LabelSize), 

legend({'u_x','u_y','u_z'},'Location','NE')
set(gca,'TickLabelInterpreter', 'latex','FontSize',TickSize)










