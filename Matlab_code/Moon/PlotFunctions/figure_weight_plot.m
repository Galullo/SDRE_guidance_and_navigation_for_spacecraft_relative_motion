% plot weight function

%% Figure parameters
TickSize = 15;
LabelSize = 20;
LineSize = 3;
LegendSize = 16;



X = linspace(-NORM_POS(20), NORM_POS(20));
Z = linspace(-NORM_POS(20), NORM_POS(20));
[X1, Z1] = meshgrid(X,Z);

figure,
surfc(INVNORM_POS(X1),INVNORM_POS(Z1),INVNORM_POS(cone_function(X1,Z1, Cz))), hold on,
%surfc(INVNORM_POS(X1),INVNORM_POS(Z1),INVNORM_POS(weight_function(X1,Z1, Cz)))
title('\textbf{Keep Out Zone (Weight Function)}','Interpreter','latex', 'FontSize', LabelSize),
xlabel('V-bar [km]','Interpreter','latex', 'FontSize', LabelSize),
ylabel('R-bar [km]', 'Interpreter','latex', 'FontSize', LabelSize), 
zlabel('$\mathbf{f}_{out}\mathbf{(x,z)}$', 'Interpreter','latex', 'FontSize', LabelSize),
set(gca,'TickLabelInterpreter', 'latex','FontSize',TickSize,...
    'XAxisLocation', 'origin', 'YAxisLocation', 'origin')


figure,
surfc(INVNORM_POS(X1),INVNORM_POS(Z1),INVNORM_POS(weight_function(X1,Z1, Cz)))
title('\textbf{Cone Axis (Weight Function)}','Interpreter','latex', 'FontSize', LabelSize),
xlabel('V-bar [km]','Interpreter','latex', 'FontSize', LabelSize),
ylabel('R-bar [km]', 'Interpreter','latex', 'FontSize', LabelSize), 
zlabel('$\mathbf{f}_{axis}\mathbf{(x,z)}$', 'Interpreter','latex', 'FontSize', LabelSize),
set(gca,'TickLabelInterpreter', 'latex','FontSize',TickSize,...
    'XAxisLocation', 'origin', 'YAxisLocation', 'origin')