TestDist = linspace(5,20,6);
TestPerDist = 20;

Ie_rho = zeros(TestPerDist,6);
Ie_rhod = zeros(TestPerDist,6);
Ie_x = (zeros(TestPerDist,6));
Ie_y = (zeros(TestPerDist,6));
Ie_z = (zeros(TestPerDist,6));
Ie_vx = (zeros(TestPerDist,6));
Ie_vy = (zeros(TestPerDist,6));
Ie_vz = (zeros(TestPerDist,6));


folder = 'WeakConstraint/';
% Load Data
file = dir([folder '*.mat']);

for n_rho = 1 : length(file)
    
    load([folder file(n_rho).name]); 
    for n_test = 1 : TestPerDist
        
        err = NORM_POS(TestData(n_test).rel_state(:,1:3) - TestData(n_test).rel_state_est(:,1:3));
        errd = NORM_VEL(TestData(n_test).rel_state(:,4:6) - TestData(n_test).rel_state_est(:,4:6));
        rho_err_norm = zeros(length(err),1);
        rhod_err_norm = zeros(length(errd),1);
        
        for j = 1:length(err)
            rho_err_norm(j) = norm(err(j,1:3));
            rhod_err_norm(j) = norm(errd(j,1:3));
        end
        time = NORM_TIME(TestData(n_test).time*60);
        
        Ie_rho(n_test,n_rho) = trapz(time(1:length(rho_err_norm)),rho_err_norm);
        Ie_rhod(n_test,n_rho) = trapz(time(1:length(rho_err_norm)),rhod_err_norm);
        Ie_x(n_test,n_rho) = TestData(n_test).IAE_state(end,1);
        Ie_y(n_test,n_rho) = TestData(n_test).IAE_state(end,2);
        Ie_z(n_test,n_rho) = TestData(n_test).IAE_state(end,3);
        Ie_vx(n_test,n_rho) = TestData(n_test).IAE_state(end,4);
        Ie_vy(n_test,n_rho) = TestData(n_test).IAE_state(end,5);
        Ie_vz(n_test,n_rho) = TestData(n_test).IAE_state(end,6);
    end
end