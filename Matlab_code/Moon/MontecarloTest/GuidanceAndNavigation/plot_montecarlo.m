% Figure parameters
TickSize = 16;
LabelSize = 18;
LineSize = 3;
LegendSize = 16;

folder = 'WeakConstraint/';
% Load Data
file = dir([folder '*.mat']);

TestDist = linspace(5,20,6);
TestPerDist = 20;
%------------------------
azimut1 = 0:0.01:2*pi;
azimut2 = 155*(pi/180):0.01:205*(pi/180);
[cx, cy, cz] = sph2cart(azimut1, zeros(size(azimut1)), 15*ones(size(azimut1)));
[cx1, cy1, cz1] = sph2cart(azimut2, zeros(size(azimut2)), 15*ones(size(azimut2)));
%-------------------------
% relative motion in LVLH frame
figure, title(['\textbf{Relative motion in LVLH} ' ],'Interpreter','latex', 'FontSize', LabelSize),
hold on, grid on, box on, %axis equal
plot3(cx,cy,cz,'k--','LineWidth',LineSize),
plot3(cx1,cy1,cz1,'w','LineWidth',LineSize),
line(-15*[0 Cz(1,4)],15*[0, Cz(1,6)],'Color','k', 'LineStyle', '--', 'LineWidth', LineSize)
line(15*[0 Cz(2,4)],-15*[0, Cz(2,6)],'Color','k', 'LineStyle', '--', 'LineWidth', LineSize)


for n_rho = length(file): -1: 1
    
    load([folder file(n_rho).name]);
    
    for n_test = 1:TestPerDist
        plot3(TestData(n_test).rel_state(1,1), TestData(n_test).rel_state(1,3), TestData(n_test).rel_state(1,2),'ro','LineWidth', LineSize/3),
        plot3(TestData(n_test).rel_state(:,1), TestData(n_test).rel_state(:,3), TestData(n_test).rel_state(:,2),'r','LineWidth', LineSize/3),
    end

plot3(0 ,0 , 0, 'b*','LineWidth',LineSize),
xlim([-21 20]), ylim([-20 20])
xlabel('V-bar [km]','Interpreter','latex', 'FontSize', LabelSize,'Position',[15, 0 ,0]),
ylabel('R-bar [km]', 'Interpreter','latex', 'FontSize', LabelSize),
zlabel('H-bar [km]', 'Interpreter','latex', 'FontSize', LabelSize),
set(gca,'TickLabelInterpreter', 'latex','FontSize',TickSize,...
    'XAxisLocation', 'origin', 'YAxisLocation', 'origin')
end

Ie_rho = zeros(TestPerDist,6);
Ie_rhod = zeros(TestPerDist,6);
tof = zeros(TestPerDist,6);
control_index = zeros(TestPerDist,6);
for n_rho = 1 : length(file)
    
    load([folder file(n_rho).name]);
    
    for n_test = 1 : TestPerDist
        tof(n_test,n_rho) = TestData(n_test).time(end);
        control_index(n_test,n_rho) = TestData(n_test).delta_v(end);
        Ie_rho(n_test,n_rho) = TestData(n_test).IAE_rho(end);
        Ie_rhod(n_test,n_rho) = TestData(n_test).IAE_rhod(end);
    end
end

figure, 
errorbar(TestDist, mean(tof), std(tof),'b--o','LineWidth', 2,...
    'MarkerSize',5,'MarkerEdgeColor','red','MarkerFaceColor','red'),
xlabel('$\rho$ [km]', 'Interpreter','latex', 'FontSize', LabelSize), xlim([0,25]),
ylabel('$t_{of}$ [min]', 'Interpreter','latex', 'FontSize', LabelSize),grid on
set(gca,'TickLabelInterpreter', 'latex','FontSize',TickSize,...
    'XAxisLocation', 'origin', 'YAxisLocation', 'origin')
title({'\textbf{Time of Flight (mean and}';'\textbf{standard deviation) with varying $\rho$}'},'Interpreter','latex', 'FontSize', LabelSize),

figure, 
errorbar(TestDist, mean(control_index),std(control_index) ,'b--o','LineWidth', 2,...
    'MarkerSize',6,'MarkerEdgeColor','blue','MarkerFaceColor','blue'),
xlabel('$\rho$ [km]', 'Interpreter','latex', 'FontSize', LabelSize), xlim([0,25]),ylim([0,40])
ylabel('$\delta_v$ [m/s]', 'Interpreter','latex', 'FontSize', LabelSize),grid on
set(gca,'TickLabelInterpreter', 'latex','FontSize',TickSize,...
    'XAxisLocation', 'origin', 'YAxisLocation', 'origin')
title({'\textbf{\emph{Control Usage Index} (mean and}'; '\textbf{standard deviation) with varying $\rho$}'},'Interpreter','latex', 'FontSize', LabelSize),









