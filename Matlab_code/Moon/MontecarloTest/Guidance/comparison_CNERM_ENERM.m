% comparison CNERM and ENERM

% Figure parameters
TickSize = 12;
LabelSize = 18;
LineSize = 3;
LegendSize = 16;

folder = {'Comparison/CheapAndSlow/CNERM/', 'Comparison/CheapAndSlow/ENERM/'};

tof_C = zeros(20,6);
control_index_C = zeros(20,6);

tof_E = zeros(20,6);
control_index_E = zeros(20,6);
 % Load Data
TestDist = linspace(5,20,6);

for n_rho = 1 : 6
    file = dir([folder{1} '*.mat']);
    
    load([folder{1} file(n_rho).name]);
    
    for n_test = 1 : 20
        tof_C(n_test,n_rho) = TestData(n_test).time(end);
        control_index_C(n_test,n_rho) = TestData(n_test).delta_v(end);
    end
end

for n_rho = 1 : 6
    file = dir([folder{2} '*.mat']);
    load([folder{2} file(n_rho).name]);
    
    for n_test = 1 : 20
        tof_E(n_test,n_rho) = TestData(n_test).time(end);
        control_index_E(n_test,n_rho) = TestData(n_test).delta_v(end);
    end
end

% plot the time of flight with CNERM and ENERM
figure, title('\textbf{Comparison Time of Flight}','Interpreter','latex', 'FontSize', LabelSize)
hold on,
errorbar(TestDist, mean(tof_C), std(tof_C),'b--o','LineWidth', 2,...
    'MarkerSize',5,'MarkerEdgeColor','red','MarkerFaceColor','red'),
errorbar(TestDist, mean(tof_E), std(tof_E),'k--o','LineWidth', 2,...
    'MarkerSize',5,'MarkerEdgeColor','green','MarkerFaceColor','green'),
xlabel('$\rho$ [km]', 'Interpreter','latex', 'FontSize', LabelSize), xlim([0,25]),
ylabel('$t_{of}$ [min]', 'Interpreter','latex', 'FontSize', LabelSize),
grid on, legend('CNERM', 'ENERM')
set(gca,'TickLabelInterpreter', 'latex','FontSize',TickSize,...
    'XAxisLocation', 'origin', 'YAxisLocation', 'origin')

% plot the time of flight with CNERM and ENERM
figure, title('\textbf{Comparison of \emph{Control Usage Index}}','Interpreter','latex', 'FontSize', LabelSize)
hold on,
errorbar(TestDist, mean(control_index_C),zeros(1,6) ,'b--o','LineWidth', 2,...
    'MarkerSize',6,'MarkerEdgeColor','red','MarkerFaceColor','red'),
errorbar(TestDist, mean(control_index_E),zeros(1,6) ,'k--o','LineWidth', 2,...
    'MarkerSize',6,'MarkerEdgeColor','green','MarkerFaceColor','green'),
grid on, legend('CNERM', 'ENERM')
xlabel('$\rho$ [km]', 'Interpreter','latex', 'FontSize', LabelSize), xlim([0,25]),
ylabel('$delta_v$ [m/s]', 'Interpreter','latex', 'FontSize', LabelSize),
set(gca,'TickLabelInterpreter', 'latex','FontSize',TickSize,...
    'XAxisLocation', 'origin', 'YAxisLocation', 'origin')

% Plot differences
figure, title('\textbf{Difference (C - E): Time of Flight}','Interpreter','latex', 'FontSize', LabelSize)
hold on, grid on,
errorbar(TestDist, mean(tof_C)-mean(tof_E), zeros(1,6),'b--o','LineWidth', 2,...
    'MarkerSize',5,'MarkerEdgeColor','red','MarkerFaceColor','red'),
xlabel('$\rho$ [km]', 'Interpreter','latex', 'FontSize', LabelSize), xlim([0,25]),
ylabel('$t_{of}$ [min]', 'Interpreter','latex', 'FontSize', LabelSize),grid on, 
set(gca,'TickLabelInterpreter', 'latex','FontSize',TickSize,...
    'XAxisLocation', 'origin', 'YAxisLocation', 'origin')

figure, title('\textbf{Difference (C - E): \emph{Control Usage Index}}','Interpreter','latex', 'FontSize', LabelSize)
hold on, grid on
errorbar(TestDist, mean(control_index_C) - mean(control_index_E),zeros(1,6) ,'b--o','LineWidth', 2,...
    'MarkerSize',6,'MarkerEdgeColor','red','MarkerFaceColor','red'),
xlabel('$\rho$ [km]', 'Interpreter','latex', 'FontSize', LabelSize), xlim([0,25]),
ylabel('$delta_v$ [m/s]', 'Interpreter','latex', 'FontSize', LabelSize),grid on, 
set(gca,'TickLabelInterpreter', 'latex','FontSize',TickSize,...
    'XAxisLocation', 'origin', 'YAxisLocation', 'origin')
