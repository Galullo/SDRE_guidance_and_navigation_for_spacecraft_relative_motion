% Figure parameters
TickSize = 12;
LabelSize = 18;
LineSize = 3;
LegendSize = 16;

% Load Data
file = dir(['*.mat']);

for n_rho = length(file): -1: 1
    
    load(file(n_rho).name);
% relative motion in LVLH frame
figure, title(['\textbf{Relative motion in LVLH} ' ],'Interpreter','latex', 'FontSize', LabelSize),
hold on, grid on, box on, %axis equal
% line(-15*[0 Cz(1,4)],15*[0, Cz(1,6)],'Color','k', 'LineStyle', '--', 'LineWidth', 2)
% line(15*[0 Cz(2,4)],-15*[0, Cz(2,6)],'Color','k', 'LineStyle', '--', 'LineWidth', 2)

    
    for n_test = 1:20
        plot3(TestData(n_test).rel_state(1,1), TestData(n_test).rel_state(1,3), TestData(n_test).rel_state(1,2),'ro','LineWidth', LineSize/3),
        plot3(TestData(n_test).rel_state(:,1), TestData(n_test).rel_state(:,3), TestData(n_test).rel_state(:,2),'r','LineWidth', LineSize/3),
    end

plot3(0 ,0 , 0, 'b*','LineWidth',LineSize),
xlim([-25 5]), ylim([-15 15])
xlabel('V-bar [km]','Interpreter','latex', 'FontSize', LabelSize,'Position',[-21, 0 ,0]),
ylabel('R-bar [km]', 'Interpreter','latex', 'FontSize', LabelSize),
zlabel('H-bar [km]', 'Interpreter','latex', 'FontSize', LabelSize),
set(gca,'TickLabelInterpreter', 'latex','FontSize',TickSize,...
    'XAxisLocation', 'origin', 'YAxisLocation', 'origin')
end

tof = zeros(20,5);
for n_rho = 1 : length(file)
    
    load(file(n_rho).name);
    
    for n_test = 1 : 20
        tof(n_test,n_rho) = TestData(n_test).time(end);
    end
end
figure, title('\textbf{mean and std for each \rho}','Interpreter','latex', 'FontSize', LabelSize)
grid on
errorbar(linspace(5, 20, 6), mean(tof), std(tof),'b--o','LineWidth', 2,...
    'MarkerSize',5,'MarkerEdgeColor','red','MarkerFaceColor','red'),
xlabel('$\rho$ [km]', 'Interpreter','latex', 'FontSize', LabelSize), xlim([0,25]),
ylabel('$t_{of}$ [min]', 'Interpreter','latex', 'FontSize', LabelSize),
set(gca,'TickLabelInterpreter', 'latex','FontSize',TickSize,...
    'XAxisLocation', 'origin', 'YAxisLocation', 'origin')










