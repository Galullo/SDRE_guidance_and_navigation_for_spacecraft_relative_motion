h = [];
h_norm = [];
for k = 1 : length(NROL1SouthTS.r.Time)
    
    if NROL1SouthTS.r.Time(k) >= NROL1SouthTS.PeriseleneTimes(1) ...
            && NROL1SouthTS.r.Time(k) <= NROL1SouthTS.PeriseleneTimes(2)
    
        h = [h; cross(INVNORM_POS(NROL1SouthTS.r.Data(k,:)),INVNORM_VEL(NROL1SouthTS.rd_M.Data(k,:)))];
        h_norm = [h_norm; norm(cross(INVNORM_POS(NROL1SouthTS.r.Data(k,:)),INVNORM_VEL(NROL1SouthTS.rd_M.Data(k,:))))];
    end
end

