addpath ..
addpath ../RelativeMotion/
addpath ../GeneralMath/

%% Load NRO orbit

% Load NRO L1 south orbit
Import_NROL1South;


%% Simulations set up

% Integrator parameters
OdeOptions = odeset('MaxStep', NROL1SouthTS.T/1e3, 'AbsTol', 1e-20, 'RelTol', 1e-13);

% Max time span to evaluate
SimTime = NORM_TIME(12*hour);

% First and second periselene passage
tp1 = NROL1SouthTS.r.Time(PeriseleneIdxs(1));
tp2 = NROL1SouthTS.r.Time(PeriseleneIdxs(2));

% Isolate first orbit
Orbit.r = getsampleusingtime(NROL1SouthTS.r, tp1, tp2+SimTime);
Orbit.rd_M = getsampleusingtime(NROL1SouthTS.rd_M, tp1, tp2+SimTime);
Orbit.T = tp2 - tp1;

% Simulate Moon from 0 to second periselene passage plus time span to
% evaluate

% Moon initial 
f0 = interp1(MoonTS.f.Time, MoonTS.f.Data, 0);
    
MoonTS = SimulateSecondaryMotion(f0, [0 tp2+SimTime], R3BParameters, OdeOptions);
disp('Moon motion simulation completed')

% Orbit starting points number
PointsNum = 100;

ErrorCR3BP6h = zeros(PointsNum,1);
ErrorER3BP6h = zeros(PointsNum,1);
ErrorKeplerian6h = zeros(PointsNum,1);
ErrorCR3BP12h = zeros(PointsNum,1);
ErrorER3BP12h = zeros(PointsNum,1);
ErrorKeplerian12h = zeros(PointsNum,1);

ErrorKeplerianwrtCR3BP6h = zeros(PointsNum,1);
ErrorKeplerianwrtER3BP6h = zeros(PointsNum,1);
ErrorKeplerianwrtCR3BP12h = zeros(PointsNum,1);
ErrorKeplerianwrtER3BP12h = zeros(PointsNum,1);

EMRatio_t0 = zeros(PointsNum,1);

MeanAnomalies = linspace(0, 2*pi, PointsNum);

% Plot first NRO orbit
figure, box on, grid on, axis equal, hold on
plot3(INVNORM_POS(Orbit.r.Data(:,1)),INVNORM_POS(Orbit.r.Data(:,2)),INVNORM_POS(Orbit.r.Data(:,3)))

for k = 1 : PointsNum
    k
    
    % Initial time, position, and velocity
    t = tp1 + MeanAnomalies(k)*Orbit.T/(2*pi);
    r0 = interp1(NROL1SouthTS.r.Time, NROL1SouthTS.r.Data, t)';
    rd_M0 = interp1(NROL1SouthTS.rd_M.Time, NROL1SouthTS.rd_M.Data, t)';
    
    % Earth-Moon gravitational pull ratio at t0
    r_em0 = interp1(MoonTS.r.Time, MoonTS.r.Data, t)';
    EMRatio_t0(k) = (1-R3BParameters.mu)*norm(r0)^2/(R3BParameters.mu*norm(r0+r_em0)^2);
    
    % Simulate motion w/ CR3BP and ER3BP equations
    CR3BP = SimulateSpacecraftMotion(r0, rd_M0, MoonTS, 'CR3BP', [t t+SimTime], R3BParameters, OdeOptions);
    ER3BP = SimulateSpacecraftMotion(r0, rd_M0, MoonTS, 'ER3BP', [t t+SimTime], R3BParameters, OdeOptions);
    Keplerian = SimulateSpacecraftMotion(r0, rd_M0, MoonTS, 'KeplerianSecondary', [t t+SimTime], R3BParameters, OdeOptions);
   
    % Plot resulting orbits
    plot3(INVNORM_POS(CR3BP.r.Data(:,1)),INVNORM_POS(CR3BP.r.Data(:,2)),INVNORM_POS(CR3BP.r.Data(:,3)),'r')
    plot3(INVNORM_POS(ER3BP.r.Data(:,1)),INVNORM_POS(ER3BP.r.Data(:,2)),INVNORM_POS(ER3BP.r.Data(:,3)),'g')
    plot3(INVNORM_POS(Keplerian.r.Data(:,1)),INVNORM_POS(Keplerian.r.Data(:,2)),INVNORM_POS(Keplerian.r.Data(:,3)),'k')
    
    % Evaluate spacecraft position after 6h and 12h
    NROend6 = interp1(NROL1SouthTS.r.Time, NROL1SouthTS.r.Data, t+NORM_TIME(6*hour))';
    NROend12 = interp1(NROL1SouthTS.r.Time, NROL1SouthTS.r.Data, t+NORM_TIME(12*hour))';
    CR3BPend6 = interp1(CR3BP.r.Time, CR3BP.r.Data, t+NORM_TIME(6*hour))';
    CR3BPend12 = interp1(CR3BP.r.Time, CR3BP.r.Data, t+NORM_TIME(12*hour))';
    ER3BPend6 = interp1(ER3BP.r.Time, ER3BP.r.Data, t+NORM_TIME(6*hour))';
    ER3BPend12 = interp1(ER3BP.r.Time, ER3BP.r.Data, t+NORM_TIME(12*hour))';
    KeplerianEnd6 = interp1(Keplerian.r.Time, Keplerian.r.Data, t+NORM_TIME(6*hour))';
    KeplerianEnd12 = interp1(Keplerian.r.Time, Keplerian.r.Data, t+NORM_TIME(12*hour))';
    
    % Compute error
    ErrorCR3BP6h(k) = norm(CR3BPend6 - NROend6);
    ErrorER3BP6h(k) = norm(ER3BPend6 - NROend6);
    ErrorKeplerian6h(k) = norm(KeplerianEnd6 - NROend6);
    ErrorCR3BP12h(k) = norm(CR3BPend12 - NROend12);
    ErrorER3BP12h(k) = norm(ER3BPend12 - NROend12);
    ErrorKeplerian12h(k) = norm(KeplerianEnd12 - NROend12);
    
    ErrorKeplerianwrtER3BP6h(k) = norm(KeplerianEnd6 - ER3BPend6);
    ErrorKeplerianwrtER3BP12h(k) = norm(KeplerianEnd12 - ER3BPend12);
    ErrorKeplerianwrtCR3BP6h(k) = norm(KeplerianEnd6 - CR3BPend6);
    ErrorKeplerianwrtCR3BP12h(k) = norm(KeplerianEnd12 - CR3BPend12);
end

% Plot results
figure, hold on, grid on
plot(radtodeg(MeanAnomalies), INVNORM_POS(ErrorCR3BP6h), 'r')
plot(radtodeg(MeanAnomalies), INVNORM_POS(ErrorER3BP6h), 'b')
plot(radtodeg(MeanAnomalies), INVNORM_POS(ErrorKeplerian6h), 'k')
plot(radtodeg(MeanAnomalies), INVNORM_POS(ErrorCR3BP12h), 'r--')
plot(radtodeg(MeanAnomalies), INVNORM_POS(ErrorER3BP12h), 'b--')
plot(radtodeg(MeanAnomalies), INVNORM_POS(ErrorKeplerian12h), 'k--')
legend('CR3BP 6h','ER3BP 6h','Keplerian 6h','CR3BP 12h','ER3BP 12h','Keplerian 12h')
xlabel('Mean anomaly [deg]')
ylabel('Error at t_f wrt assigned orbit [km]')
xlim([0 360])

figure, hold on, grid on
plot(radtodeg(MeanAnomalies), INVNORM_POS(ErrorKeplerianwrtCR3BP6h), 'r')
plot(radtodeg(MeanAnomalies), INVNORM_POS(ErrorKeplerianwrtER3BP6h), 'b')
plot(radtodeg(MeanAnomalies), INVNORM_POS(ErrorKeplerianwrtCR3BP12h), 'r--')
plot(radtodeg(MeanAnomalies), INVNORM_POS(ErrorKeplerianwrtER3BP12h), 'b--')
legend('wrt CR3BP 6h',' wrt ER3BP 6h','wrt CR3BP 12h','wrt ER3BP 12h')
xlabel('Mean anomaly [deg]')
ylabel('Error at t_f of Keplerian [km]')
xlim([0 360])

figure, hold on, grid on
plot(radtodeg(MeanAnomalies), EMRatio_t0)
xlabel('Mean anomaly [deg]')
ylabel('Earth-Moon gravitational pull ratio at t_0')
xlim([0 360])
