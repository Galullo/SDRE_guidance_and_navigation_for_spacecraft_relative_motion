%% Load NRO L1 South orbit and Moon information

addpath ..
Parameters_EarthMoon

% Load orbit, in Moon co-rotating coordinate system (x-axis pointing the Earth,
% z normal to the Earth-Moon orbital plane)
%   NROL1South_Time : Time [s]
%   NROL1South_X : x [km]
%   NROL1South_Y : y [km]
%   NROL1South_Z : z [km]
%   NROL1South_Vx : velocity along x [km/s]
%   NROL1South_Vy : velocity along y [km/s]
%   NROL1South_Vz : velocity along z [km/s]
load NROL1South.mat

%% Compute approximate orbit period

Period = zeros(6,1);

[~,locs] = findpeaks(NROL1South_X);
Period(1) = mean(diff(NROL1South_Time(locs)));

[~,locs] = findpeaks(NROL1South_Y);
Period(2) = mean(diff(NROL1South_Time(locs)));

[~,locs] = findpeaks(NROL1South_Z);
Period(3) = mean(diff(NROL1South_Time(locs)));

[~,locs] = findpeaks(NROL1South_Vx);
Period(4) = mean(diff(NROL1South_Time(locs)));

[~,locs] = findpeaks(NROL1South_Vy);
Period(5) = mean(diff(NROL1South_Time(locs)));

[~,locs] = findpeaks(NROL1South_Vz);
Period(6) = mean(diff(NROL1South_Time(locs)));

Period = mean(Period);

clear locs

%% Find periselene times

DistFromMoon = sum([NROL1South_X, NROL1South_Y, NROL1South_Z].^2, 2).^.5;
[~, PeriseleneIdxs] = findpeaks(-DistFromMoon);


%% Create orbit structure

NROL1SouthTS.r = ...
    timeseries( ...
        NORM_POS([NROL1South_X, NROL1South_Y, NROL1South_Z]), ...
        NORM_TIME(NROL1South_Time), ...
        'Name', 'S/C position in M' ...
    );
NROL1SouthTS.r.DataInfo.Units = 'a';
NROL1SouthTS.r.TimeInfo.Units = 'n';
    
NROL1SouthTS.rd_M = ...
    timeseries( ...
        NORM_VEL([NROL1South_Vx, NROL1South_Vy, NROL1South_Vz]), ...
        NORM_TIME(NROL1South_Time), ...
        'Name', 'S/C velocity in M' ...
    );
NROL1SouthTS.rd_M.DataInfo.Units = 'km/s';
NROL1SouthTS.rd_M.TimeInfo.Units = 's';

NROL1SouthTS.T = NORM_TIME(Period);
NROL1SouthTS.PeriseleneTimes = NORM_TIME(NROL1South_Time(PeriseleneIdxs));

% Moon true anomaly in [deg] wrt Earth from July 1, 2020. Obtained from
% JPL's HORIZON system (https://ssd.jpl.nasa.gov/horizons.cgi).
load MoonEphemeris.mat

% Set t0 = 0, time in seconds, and true anomaly in radians
MoonTS.f = timeseries(...
    degtorad(MoonTrueAnomaly_deg), ...
    NORM_TIME((MoonTime_JulianDayNumber - MoonTime_JulianDayNumber(1))*day)...
    );

clear MoonTime_JulianDayNumber MoonTrueAnomaly_deg


%% Plot orbit
figure('Color', [1 1 1]), hold on
plot3(NROL1South_X*1e-4, NROL1South_Y*1e-4, NROL1South_Z*1e-4, 'LineWidth', 2)
plot3(0, 0, 0, 'ko', 'MarkerSize',10, 'LineWidth', 2)
grid on, axis equal, box on
xlim([-1 2]), ylim([-2.5 2.5]), 
xlabel('$x$ [$10^4$ km]','FontSize',18','Interpreter','latex')
ylabel('$y$ [$10^4$ km]','FontSize',18','Interpreter','latex')
zlabel('$z$ [$10^4$ km]','FontSize',18','Interpreter','latex')
set(gca,'TickLabelInterpreter', 'latex','FontSize',15)
hold on
for k = PeriseleneIdxs'
    
    t0 = NROL1South_Time(k);
    %t_init = t0 + degtorad(100)*Period/(2*pi);
    %t_end = t_init + degtorad(160)*Period/(2*pi);

    t_init = t0 + degtorad(0)*Period/(2*pi);
    t_end = t_init + degtorad(5)*Period/(2*pi);

    Rendezvous = getsampleusingtime(NROL1SouthTS.r, NORM_TIME(t_init), NORM_TIME(t_end));
    plot3(INVNORM_POS(Rendezvous.Data(:,1))*1e-4,INVNORM_POS(Rendezvous.Data(:,2))*1e-4,INVNORM_POS(Rendezvous.Data(:,3))*1e-4,'r','LineWidth',2)
end