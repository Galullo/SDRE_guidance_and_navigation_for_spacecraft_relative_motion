% References
% 
% [1] Folkner, W., et al., The Planetary and Lunar Ephemerides DE430 and
% DE431, IPN Progress Report 42-196, 2014.
% https://ipnpr.jpl.nasa.gov/progress_report/42-196/196C.pdf
%
% [2] Yoder, C. F., Astrometric and geodetic properties of Earth and the
% Solar system, Global Earth Physics: A Handbook of Physical Constants,
% edited by T. Ahrens, American Geophysical Union, 1995.

% =========================================================================
% Time constants
% =========================================================================

min   = 60;
hour  = 60*min;
day   = 24*hour;
month = 30*day;

% =========================================================================
% Gravitational parameters
% =========================================================================

% mu_m : Moon gravitational parameter [km^3/s^2], Ref.[1]
% mu_e : Earth gravitational parameter [km^3/s^2], Ref.[1]

mu_m = 4902.800066;
mu_e = 398600.435436;

% =========================================================================
% Moon elliptic orbit
% =========================================================================

% a : semi-major axis [km], Ref.[2]
% e : eccentricity, Ref.[2]
% p : semilatus rectum [km]
% n : mean motion [rad/s]
% T : Moon orbital period [s]

MoonOrbit.a = 384400;               
MoonOrbit.e = 0.05490;              
MoonOrbit.p = MoonOrbit.a*(1 - MoonOrbit.e^2);
MoonOrbit.n = sqrt(mu_e/MoonOrbit.a^3);        
MoonOrbit.T = 2*pi/MoonOrbit.n;                         

% =========================================================================
% Anonymous functions for normalization
% =========================================================================

% From s to normalized time
NORM_TIME = @(x)(x*MoonOrbit.n);

% From km, km/s, km/s^2 to normalized units
NORM_POS = @(x)(x./MoonOrbit.a);
NORM_VEL = @(x)(x./(MoonOrbit.a*MoonOrbit.n));
NORM_ACC = @(x)(x./(MoonOrbit.a*MoonOrbit.n^2));

% From rad/s, rad/s^2 to normalized units
NORM_ANGV = @(x)(x./MoonOrbit.n);
NORM_ANGA = @(x)(x./MoonOrbit.n^2);

% From normalized units to s, km, km/s, km/s^2, rad/s rad/s^2
INVNORM_TIME    = @(x)(x./MoonOrbit.n);
INVNORM_POS     = @(x)(x.*MoonOrbit.a);
INVNORM_VEL     = @(x)(x.*(MoonOrbit.a*MoonOrbit.n));
INVNORM_ACC     = @(x)(x.*(MoonOrbit.a*MoonOrbit.n^2));
INVNORM_ANGV    = @(x)(x.*MoonOrbit.n);
INVNORM_ANGA    = @(x)(x.*MoonOrbit.n^2);


% =========================================================================
% Restricted three-body problem parameters
% =========================================================================

R3BParameters.a = NORM_POS(MoonOrbit.a);               
R3BParameters.e = MoonOrbit.e;              
R3BParameters.p = NORM_POS(MoonOrbit.p);
R3BParameters.n = NORM_ANGV(MoonOrbit.n);        
R3BParameters.T = NORM_TIME(MoonOrbit.T);

% Mass ratio
R3BParameters.mu = mu_m/(mu_e + mu_m);