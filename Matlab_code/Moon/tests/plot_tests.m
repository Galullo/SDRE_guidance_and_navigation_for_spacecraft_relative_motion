% This script plot the test results;

%% Figure parameters
TickSize = 16;
LabelSize = 18;
LineSize = 3;
LegendSize = 16;

%% Load file
%folder = {'CNERM_lento', 'ENERM_lento', 'CNERM_veloce', 'ENERM_veloce'};
folder = {'CNERM_veloce', 'CNERM_lento'};

ctrl_eng_index = zeros(4, 16);
%test_data = struct(folder{1},[],folder{2},[],folder{3},[],folder{4},[]);
test_data = struct(folder{1},[],folder{2},[]);

%------------------------
azimut1 = 0:0.01:2*pi;
azimut2 = 155*(pi/180):0.01:205*(pi/180);
[cx, cy, cz] = sph2cart(azimut1, zeros(size(azimut1)), 20*ones(size(azimut1)));
[cx1, cy1, cz1] = sph2cart(azimut2, zeros(size(azimut2)), 20*ones(size(azimut2)));
%-------------------------
% relative motion in LVLH frame

tof = zeros(4,16);

for k = 1:length(folder)%:-1:1
    file = dir([folder{k} '/*.mat']);
    
    
    figure, title('\textbf{Relative motion in LVLH}','Interpreter','latex', 'FontSize', LabelSize),
    hold on, grid on, box on, %axis equal
    plot3(cx,cy,cz,'k--','LineWidth',LineSize),
    plot3(cx1,cy1,cz1,'w','LineWidth',LineSize),
    line(-20*[0 Cz(1,4)],20*[0, Cz(1,6)],'Color','k', 'LineStyle', '--', 'LineWidth', LineSize)
    line(20*[0 Cz(2,4)],-20*[0, Cz(2,6)],'Color','k', 'LineStyle', '--', 'LineWidth', LineSize)

    for i = 1:length(file)
        
        load([folder{k} '/' file(i).name])
        
        % time in min
        time_nn = INVNORM_TIME(time)./min;              % min
        tof(k,i) = time_nn(end);
        % relative position and velocity
        rho_ER3BP_nn = INVNORM_POS(rho_sim);            % [km]
        rhod_ER3BP_nn = INVNORM_VEL(rhod_sim)*1e3;      % [m/s]
        
        % plot data
        plot3(rho_ER3BP_nn(1,1), rho_ER3BP_nn(1,3),rho_ER3BP_nn(1,2),'ro','LineWidth', LineSize),
        plot3(rho_ER3BP_nn(:,1), rho_ER3BP_nn(:,3),rho_ER3BP_nn(:,2),'r','LineWidth', LineSize),
        
        % Control law
        u_L_nn = INVNORM_ACC(u_L)*1e3;                  % [m/s^2]
        
        % Control Energy Index
        
        ctrl_eng = zeros(length(u_L_nn),1);
        
        for j = 1:length(u_L_nn)
            ctrl_eng(j) = norm(u_L_nn(j,:));
        end
        test_data.(folder{k}).(['acc_' num2str(i)]) = ctrl_eng;
        test_data.(folder{k}).(['vel_' num2str(i)]) = rhod_ER3BP_nn;
        test_data.(folder{k}).(['time_' num2str(i)]) = time_nn;
        
        ctrl_eng_index(k,i) = trapz(INVNORM_TIME(time), ctrl_eng);
    end
    
    plot3(0 ,0 , 0, 'b*','LineWidth',LineSize),
    
    xlim([-25 5]), ylim([-15 15])
    xlabel('V-bar [km]','Interpreter','latex', 'FontSize', LabelSize,'Position',[-19, 0 ,0]),
    ylabel('R-bar [km]', 'Interpreter','latex', 'FontSize', LabelSize),
    zlabel('H-bar [km]', 'Interpreter','latex', 'FontSize', LabelSize),
    set(gca,'TickLabelInterpreter', 'latex','FontSize',TickSize,...
        'XAxisLocation', 'origin', 'YAxisLocation', 'origin')
        
end



% %% Energy Indexs
% % the difference between index of CNERM and ENERM (1 - lento; 2 - veloce;)
% figure, title('\textbf{Control Energy Index lento}','Interpreter','latex', 'FontSize', LabelSize),
% hold on, grid on, box on, %axis equal
% stem(ctrl_eng_index(2,:) - ctrl_eng_index(1,:),'b', 'LineWidth', LineSize)
% ylabel('energy Elliptic - energy Circular [km/s]'), xlabel('n. test')
% set(gca,'TickLabelInterpreter', 'latex','FontSize',TickSize)
% 
% figure, title('\textbf{Control Energy Index veloce}','Interpreter','latex', 'FontSize', LabelSize),
% hold on, grid on, box on, %axis equal
% stem(ctrl_eng_index(4,:) - ctrl_eng_index(3,:),'b', 'LineWidth', LineSize)
% ylabel('energy Elliptic - energy Circular [km/s]'), xlabel('n. test')
% set(gca,'TickLabelInterpreter', 'latex','FontSize',TickSize)
% 
% % plot the acceleration norm vector
% for i = 1:length(folder)
%     figure, title(['\textbf{Control Law} ' char(folder{i})],'Interpreter','latex', 'FontSize', LabelSize),
%     hold on, grid on, box on,
%     for j = 1:length(file)
%         hold on
%         plot(test_data.(folder{i}).(['time_' num2str(j)]),test_data.(folder{i}).(['acc_' num2str(j)]), 'LineWidth', LineSize)
%     end
%     ylabel('energy [m/s^2]'), xlabel('time [min]')
%     set(gca,'TickLabelInterpreter', 'latex','FontSize',TickSize)
% end


