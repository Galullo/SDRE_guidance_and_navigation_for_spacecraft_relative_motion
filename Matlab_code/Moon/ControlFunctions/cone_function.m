function y = cone_function(x, z, Ci)
% function that make a cone around the target spacecraft
% IPUTS:
% x     position;
% z     position;
% Ci    fictious output matrix
%------------------------------------------------------------------------%
%                           UTILITIES FUNCTIONS                          %
%------------------------------------------------------------------------%
%------------------------------------------------------------------------%
% Logistic Function

kstif = 1 * 10^(10);
kgain = 1 * 10^(0);

LOG_FUN = @(x)(kgain./(1 + exp(-kstif.*x)));
%------------------------------------------------------------------------%
% conic function (circle or ellipse)

% radius of circle near the origin (target) (3 m) (1 [m]) 
r_tol_weigth =  7.8044e-09; %2.5 * 10^(-9);

% conic parameters
a = 1;
b = 1;

DR = @(x,z)(sqrt((x./a).^2 + (z./b).^2) - r_tol_weigth);
%------------------------------------------------------------------------%

range = 3.9022e-05;     % 15 km 
y = (2e1) * ((sign(Ci(1,6)*x + Ci(1,4)*z) + sign(Ci(2,6)*x + Ci(2,4)*z) + 2).* LOG_FUN(DR(x, z))).* LOG_FUN(-(DR(x,z) - range));

end

