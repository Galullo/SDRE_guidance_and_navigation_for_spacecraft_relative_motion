function y = weight_function(x, z, Ci)
% this function compute the weight that encurage tha chaser to allign with
% cone axis

% handle functions
dc          = @distance_from_line_c;

% Function soft-rendevous (close the taget): 1e6 fast; 6e6 slow;
y = ((1e6)*dc(x,z,Ci));


end

function y = distance_from_line_c(x, z, Ci)

d_tol =  0;

% guadagno 0.5 per la distanza lineare
y = (d_tol + (abs(-Ci(3,6)*x + Ci(3,4)*z)./(sqrt(Ci(3,4)^2 + Ci(3,6)^2)))).^1;

end

