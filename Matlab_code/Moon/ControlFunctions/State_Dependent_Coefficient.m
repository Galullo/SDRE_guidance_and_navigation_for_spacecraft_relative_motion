function A = State_Dependent_Coefficient(X, omega_li, omegad_li, r_st, r_ps, R3BParameters)
% This function computes the State-Dependent Coefficients matrix of
% relative motion in R3BP (Restricted Three Body Problem)
%
% INPUTS
% r_ps          : distance between Earth-Moon, in LVLH frame;
% r_st          : target position w.r.t. Moon, in LVLH frame;
% mu_n          : gravitationa paraneter (normalized);
% X             : relative positions and velocities, in LVLH frame;
% omega_li      : angular velocity of LVLH frame w.r.t. inertial frame, in LVLH frame; 
% omegad_li     : angular acceleration of LVLH frame w.r.t. inertial frame, in LVLH frame;
%
% OUTPUT
% A             : SDC parametrization matrix; 

% call functions for SDC parametrization of gravitational terms
earth_term  = @earth_sdc_term;
moon_term   = @moon_sdc_term;


% SDC parametrizaton dependent on omega and omegad
A21_omega = [omega_li(2)^2 + omega_li(3)^2,   omegad_li(3) - omega_li(1)*omega_li(2), - omegad_li(2) - omega_li(1)*omega_li(3);
 - omegad_li(3) - omega_li(1)*omega_li(2),          omega_li(1)^2 + omega_li(3)^2,   omegad_li(1) - omega_li(2)*omega_li(3);
   omegad_li(2) - omega_li(1)*omega_li(3), - omegad_li(1) - omega_li(2)*omega_li(3),          omega_li(1)^2 + omega_li(2)^2];

% SDC parametrizaton dependent on moon gravitational term
A21_moon = moon_term(X, r_st, R3BParameters.mu);

% SDC parametrizaton dependent on earth gravitational term         
A21_earth = earth_term(X, r_ps, r_st, (1 - R3BParameters.mu));
      
A21 = A21_omega + A21_moon + A21_earth;

% SDC parametrization Coriolis terms
A22 = [            0,  2*omega_li(3), -2*omega_li(2);
 -2*omega_li(3),            0,  2*omega_li(1);
  2*omega_li(2), -2*omega_li(1),            0];
 


% State Dependent Coefficient Matrix
A = [zeros(3,3), eye(3); A21, A22];
end

function A_m = moon_sdc_term(X, r_st, mu_m)

% relative position
x = X(1);
y = X(2);
z = X(3);

% norm of target position w.r.t. Moon, in LVLH frame;
r_mt = norm(r_st);

% chaser position w.r.t. Moon, in LVLH frame;
r_mc = norm([x; y; (z - r_mt)]);

% algebric parameter for Moon gravitational term (usefull in SDC parametrization)
gamma_m = -mu_m*((r_mc^2 + r_mt*r_mc + r_mt^2)/((r_mc + r_mt)*(r_mc^3)*(r_mt^2)));


A_m = [-mu_m/r_mc^3,          0,           0;
                0   , -mu_m/r_mc^3,           0;
             gamma_m*x,    gamma_m*y, gamma_m*(z - r_mt) - mu_m/r_mc^3];

end

function A_e = earth_sdc_term(X, r_ps, r_st, mu_e)

% relative position
x = X(1);
y = X(2);
z = X(3);

% distance between Earth-Moon in LVLH frame
r_em_x = r_ps(1);
r_em_y = r_ps(2);
r_em_z = r_ps(3);

% norm of target position w.r.t. Moon, in LVLH frame;
r_mt = norm(r_st);
% norm of target position w.r.t. Earth, in LVLH frame;
r_et = norm(r_st) + norm(r_ps);
% chaser position w.r.t. Earth, in LVLH frame;
r_ec = norm([(r_em_x + x); (r_em_y + y); (r_em_z + z - r_mt)]);

% algebric parameters for Earth gravitational term (usefull in SDC parametrization)
gamma_e = mu_e * ((r_ec^2 + r_et*r_ec + r_et^2)/((r_ec + r_et)*(r_ec^3)*(r_et^3)));

A_e = [gamma_e*r_em_x*(2*r_em_x + x) - (mu_e)/r_ec^3, gamma_e*r_em_x*(2*r_em_y + y), gamma_e*r_em_x*(2*(r_em_z - r_mt) + z);
          gamma_e*r_em_y*(2*r_em_x + x), gamma_e*r_em_y*(2*r_em_y + y)  - (mu_e)/r_ec^3, gamma_e*r_em_y*(2*(r_em_z - r_mt) + z);
          gamma_e*(r_em_z - r_mt)*(2*r_em_x + x), gamma_e*(r_em_z - r_mt)*(2*r_em_y + y), gamma_e*(r_em_z - r_mt)*(2*(r_em_z - r_mt) + z) - (mu_e)/r_ec^3];


end