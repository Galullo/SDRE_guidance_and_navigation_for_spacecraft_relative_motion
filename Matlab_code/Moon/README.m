% one_simulation.m generate all necessary for run a simulation;

% initialization_parameter.m generete parameter for relative motion;
% Parameters_EarthMoon.m generate all parameters for Celestial Mechanincs;

% montecarlo_guidance.m run a Monte Carlo simulation for comparison between
% Circular and Elliptical assumption;

% montecarlo_guidance_and_navigation.m run a Monte Carlo simulation for
% testing the guidance and navigation system;

% DIRECTORIES
% ControlFunctions: contains useful function for controller;
% tests : contains data on how the controller works;
% PlotFunctions : contains useful script for plotting the results;
% MontecarloTests: contains simulations data;
% RelativeMotion: contains functions for simulating the relative motion;
% nro_matlab: contains Near-Rectilinera-Orbit data;
% GeneralMath: contains useful math functions;


