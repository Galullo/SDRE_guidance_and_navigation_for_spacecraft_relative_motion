% one simulation

addpath ControlFunctions
addpath nro_matlab
addpath RelativeMotion
addpath GeneralMath

%% Load Parameter
initialization_parameter;

%% Simulation parameters
SimTime = NORM_TIME(6*hour);

rho = [-10 0 -2]';

rhod = zeros(3,1);                    % km/s

X0_filter = [NORM_POS(rho + 1e-4 * rand(3,1));...
    NORM_VEL(rhod + 1e-5 * rand(3,1))];


% Compute chaser initial
r_mc0 = r_mt0 + C_MtoL0'*NORM_POS(rho);
rd_mc0 = rd_mt0 + C_MtoL0'*NORM_VEL(rhod) + C_MtoL0'*cross(omega_lm0, NORM_POS(rho));
