% Initialization parameter

%% Load Earth-Moon system parameters
Parameters_EarthMoon;

%% Load target orbit and use it for initialize target simulation
Import_NROL1South;

% Mean anomalies to test, and initial times from first perisele passage
M = 0*pi;
t0 = NROL1SouthTS.r.Time(PeriseleneIdxs(1)) + M*NROL1SouthTS.T/(2*pi);

% Orbit period as time between first and second periselene passage
OrbitPeriod = NROL1SouthTS.r.Time(PeriseleneIdxs(2)) - NROL1SouthTS.r.Time(PeriseleneIdxs(1));

%% Constraint definition

% angle of cone constraint
gamma   = 0.4363;                           % [rad] (25 deg)

% constraint coefficients for ESA - LVLH frame
C11 = cos(gamma);
C12 = sin(gamma);
C21 = -cos(gamma);
C22 = sin(gamma);
C31 = -1;
C32 = 0;

%% System Definition For Control

% input matrix
B0 = [zeros(3,3); eye(3)];

% Cz relates the system's output with system's state
Cz = [0 0 0 C11 0 C12; 0 0 0 C21 0 C22; 0 0 0 C31 0 C32];

% Dz relates the system's output with system's input
Dz = zeros(3,3);

%% Controller parameters

Q0 = [(4e5) * diag([1 10 1]), zeros(3,3); zeros(3,3), diag([1 1 1])];
R0 = (5e-8) * diag([1 1 1]);

% frequency of controller
Ts = MoonOrbit.n * 1;                       % Hz


%% Filter parameters

% Process Covariance matrix
LAMBDA = [zeros(3,3); eye(3)];
Qf =  1e0 * LAMBDA * 1e-12 * LAMBDA';

% standard deviation of sensor measurements
stdm = NORM_POS(1e-5);

% Sensor Covariance Matrix
Rf = 1e0 * stdm^2 * eye(3);

% Initial Error Covariance matrix
Pf = 1e0 * [NORM_POS(1e-4) * eye(3), zeros(3,3); zeros(3,3), NORM_VEL(1e-5) * eye(3)];


%% Initial conditions Primaries
% Moon initial true anomaly
f0 = interp1(MoonTS.f.Time, MoonTS.f.Data, t0);

% Moon initial state, angular velocity and acceleration
r_em0 = R3BParameters.p/(1 + R3BParameters.e*cos(f0)) * [-1; 0; 0];
r_em0_normd = sqrt((1-R3BParameters.mu)/R3BParameters.p)*R3BParameters.e*sin(f0);
omega_me0 = (sqrt((1-R3BParameters.mu)*R3BParameters.p)/norm(r_em0)^2)*[0; 0; 1];
omegad_me0 = -2*r_em0_normd*omega_me0/norm(r_em0);

% Target initial state
r_mt0 = interp1(NROL1SouthTS.r.Time, NROL1SouthTS.r.Data, t0)';
rd_mt0 = interp1(NROL1SouthTS.rd_M.Time, NROL1SouthTS.rd_M.Data, t0)';
rdd_mt0 = Eqs_ER3BP(r_mt0, rd_mt0, r_em0, omega_me0, omegad_me0, R3BParameters.mu);

% Coordinate change matrix M -> L at t0 and omega l/m at t0
C_MtoL0 = CoordChange_MtoL(r_mt0, rd_mt0);
omega_lm0 = Eqs_LVLHAngVel(r_mt0, rd_mt0, rdd_mt0);

