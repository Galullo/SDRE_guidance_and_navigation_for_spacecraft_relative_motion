% Montecarlo Guidance

addpath ControlFunctions
addpath PlotFunctions
addpath nro_matlab
addpath RelativeMotion
addpath GeneralMath


folder = 'MontecarloTest/Guidance/Comparison/CheapAndSlow/CNERM_hard_constraints2';
mkdir(folder)
%% Load Parameter
initialization_parameter;

%% Simulation parameters
SimTime = NORM_TIME(6*hour);

if isempty(gcp)
    parpool('local',8,'AttachedFiles',{'GeneralMath/','RelativeMotion/', 'FilterFunctions/', 'ControlFunctions/'})
end

SimulinkModel = 'moon_rendezvous';

% Execute on all workers
spmd
    current_dir = pwd;
    temp_dir = tempname;     % temporary directory random name
 
    addpath(current_dir);
    mkdir(temp_dir);
    cd(temp_dir);
 
    % Load model on worker
    load_system(SimulinkModel);
    set_param(SimulinkModel,'Solver', 'ode45','StartTime', '0', ...
        'StopTime', 'SimTime', 'MaxStep', 'auto', ...
        'AbsTol', 'auto', 'RelTol', 'auto', ...
        'SaveOutput', 'on')
    
end


%% Relative state to tests (Simulink model)


% rho norms to test
TestDist = linspace(5, 20, 6); % [km]
TestDistNum = length(TestDist);

% Number of tests for each rho
TestPerDist = 10;
for k_rho = 1: TestDistNum
    
    TestDataRho = [];
    parfor j_MC = 1 : TestPerDist
        
        assignin('base','SimTime',SimTime);
        assignin('base','R3BParameters',R3BParameters);
        assignin('base','OrbitPeriod',OrbitPeriod);
        
        assignin('base','f0',f0);
        assignin('base','r_mt0',r_mt0);
        assignin('base','rd_mt0',rd_mt0);
        assignin('base','omega_lm0',omega_lm0);
        assignin('base','C_MtoL0',C_MtoL0);
        
        assignin('base', 'Ts', Ts);
        
        assignin('base', 'B0', B0);
        assignin('base', 'Cz', Cz);
        assignin('base', 'Dz', Dz);
        
        assignin('base', 'Q0', Q0);
        assignin('base', 'R0', R0);
       

        % Create random rho with norm equal to TestDist(k_rho)
        
        azimuth = 150*(pi/180) - (150*(pi/180) - 210*(pi/180))*rand;
        elevation = 0.001*(pi/180) - (0.001*(pi/180) + 0.001*(pi/180))*rand;
        
        rho = zeros(3,1);
        [rho(1), rho(3), rho(2)] = sph2cart(azimuth, elevation, TestDist(k_rho)); % km
        
        rhod = zeros(3,1);

        rhod(1) = 1e-3 * rand;                    % km/s
        rhod(2) = 1e-3 * rand; % km/s
        rhod(3) = 1e-3 * rand; %* (sign(rho(3))); % km/s
        % Compute chaser initial
        r_mc0 = r_mt0 + C_MtoL0'*NORM_POS(rho);
        rd_mc0 = rd_mt0 + C_MtoL0'*NORM_VEL(rhod) + C_MtoL0'*cross(omega_lm0, NORM_POS(rho));
        
        assignin('base','r_mc0', r_mc0);
        assignin('base','rd_mc0', rd_mc0);
        
    
        SimOut = sim(SimulinkModel)
        
        X_tmp = SimOut.get('X');
        
        TestDataRho(j_MC).time      = INVNORM_TIME(SimOut.get('time'))./min;        % min
        TestDataRho(j_MC).delta_vt  = INVNORM_POS(SimOut.get('ctrl_index_ut'))*1e3;    % m
        TestDataRho(j_MC).delta_v   = INVNORM_VEL(SimOut.get('ctrl_index_u'))*1e3;    % m/s
        
        TestDataRho(j_MC).u         = INVNORM_ACC(SimOut.get('u_L'))*1e3;           % m/s^2
        TestDataRho(j_MC).rel_state = [INVNORM_POS(X_tmp(:,1:3)), INVNORM_VEL(X_tmp(:,4:6))]; 
        TestDataRho(j_MC).rc        = INVNORM_POS(SimOut.get('rc_sim'));            % km
        TestDataRho(j_MC).rt        = INVNORM_POS(SimOut.get('rt_sim'));            % km
        
        disp([datestr(datetime), ' Test r',num2str(k_rho),' pt:',num2str(j_MC), ' compleated;'])
        
    end
    
    TestData = eval('TestDataRho');
    
    save([folder '/test_' num2str(k_rho) '.mat'],'TestData')
    disp([datestr(datetime),' Partial result SAVED!'])
end