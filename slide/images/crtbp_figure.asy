import geometry;
unitsize(1cm);

//axis
draw((7,1)--(15,1), arrow = Arrow);  label("$x$",(14.8,0.8), p = fontsize(10pt),S);
draw((9,0.5)--(9,6), arrow = Arrow); label("$y$",(8.8,5.8), p = fontsize(10pt),W);

//point mass
dot((12,3.05)); label("$m$",(12,3.1), p = fontsize(9pt),N);

//R1 and R2 vector
draw((8.6,1)--(12,3), red, arrow = ArcArrow); label("$R_{1}$", (10.3,2),p = fontsize(9pt), NW);
draw((13.5,1)--(12,3), red, arrow = ArcArrow); label("$R_{2}$",(12.75,2),p = fontsize(9pt),NE);

//the two planet
fill(circle((8.6,1),0.25), gray); label("$m_{1}$",(8.6,1.3),p = fontsize(8pt), NW); label("(-$\mu$, 0, 0)",(8.6,0.8),p = fontsize(9pt), SW);
fill(circle((13.5,1),0.15), gray); label("$m_{2}$",(13.5,1.3),p = fontsize(8pt), NE); label("(1-$\mu$, 0, 0)",(13.5,0.8),p = fontsize(9pt), SW);
