import geometry; 
unitsize(1cm);

// define the xy-axis
draw((3,1)--(16,1), arrow = Arrow); label("$x$",(15.8,0.8), p = fontsize(10pt),S);
draw((9,0.5)--(9,7), arrow = Arrow); label("$y$",(8.8,6.8), p = fontsize(10pt),W);

// define the circular orbit
draw(circle((8.6,1),4.9),blue);

// define the lagrangian points
dot((15,1), blue); label("$L_{2}$", (15,1),p = fontsize(15pt),NE);
dot((12,1), blue); label("$L_{1}$", (12,1),p = fontsize(15pt),NW);
dot((3.7,1), blue); label("$L_{3}$", (3.7,1),p = fontsize(15pt),NW);
dot((11.05,5.25), blue); label("$L_{4}$", (11.05,5.25),p = fontsize(15pt),N);
dot((11.05,-3.25), blue); label("$L_{5}$", (11.05,-3.25),p = fontsize(15pt),S);

// vector from m1 to L4
draw((8.6,1)--(11,5.2),red, arrow = Arrow);

// angle and direction of motion
draw(arc((8.6,1), r=1, angle1=0, angle2=60),arrow=Arrow(TeXHead)); label("$60^o$",(9.4,1.5),NE);
draw(arc((8.6,1), r=4.5, angle1=205, angle2=225), blue, arrow=Arrow(TeXHead));

// define the m2 and m1
fill(circle((8.6,1),0.25), gray); label("$m_{1}$",(8.6,1.3),p = fontsize(13pt), NW); 
fill(circle((13.5,1),0.15), gray); label("$m_{2}$",(13.5,1.3),p = fontsize(13pt), NE); 
