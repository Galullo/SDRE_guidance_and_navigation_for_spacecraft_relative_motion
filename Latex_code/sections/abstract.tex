\documentclass[../main.tex]{thesis_style}
\graphicspath{{images/}{../images/}}

\begin{document}
\begin{abstract} \begin{center} \textbf{\abstractname} \end{center}
This report presents the SDRE guidance and navigational technique with state constraints that is capable of driving a spacecraft approaching a target vehicle on Near Rectilinear Orbit in the Earth-Moon system. Nonlinear varying-time relative equations were used to design the guidance and navigation system. Constraints and weight functions parameters are maintained as generic as possible, in a way to delineate a different requirements for the different missions.\\ In order to fully validate the proposed solution, the Monte Carlo simulation has been performed over a range of 5 - 20 km for the relative position, in which the chaser can begin its rendezvous maneuvers even outside the cone. The results show that the performances, evaluated in terms of maneuvers time and fuel consumption, are comparable with real mission. 
\end{abstract}

\section{Introduction} 
The rendezvous and docking (RVD) process consists of a series of orbital manoeuvres and controlled trajectories, which successively bring the active vehicle (\emph{chaser}) close and eventually into contact with the passive vehicle (\emph{target}). The complexity of the rendezvous approach results from the multitude of conditions and constraints which must be fulfilled. The target station may impose safety zones, approach-trajectory corridors and hold points along the way to check out the chaser conditions. Any dynamic state (position and velocities, attitude and angular rates) of the chaser vehicle outside the nominal limits of the approach trajectory could lead to collision with the target, a situation dangerous for crew and vehicle integrity \cite{Fehse2003}.\\ Traditionally the rendezvous and proximity maneuver have been performed using open-loop maneuver planning techniques ed ad hoc error correction. Examples of constrained maneuvers include the thrust magnitude constraints, constraints on the approaching spacecraft to maintain its position within a Line-of-Sight cone emanating from the docking port on the target platform, and constraints on the terminal translational velocity for soft-docking are proposed in \cite{Hartley2012, DiCairano2012}. However, since the 1960s, the problem of control of rendezvous in Earth's orbit has been studied \cite{Ankersen2010,Franzini2015, Franzini2016} and real missions are practiced continuously. Now another step forward in the exploration of space must be done. \\ In fact, one of the objectives of the main national space agencies is to build a modular space station close to the moon \cite{GlobRoad}. The optimal location for this space infrastructure can be a \emph{Halo orbit} in $L_1$ or $L_2$ (Lagrangian point) \cite{Zimovan2017}. In this region the classical RVD control strategies based on two-body dynamics fail.\\ Several studies aimed at the analysis on the station access by the incoming vehicles performing logistic flights, crew transportation missions, or samples return from the Moon surface are currently ongoing \cite{Murakami2015, Colagrossi2017}. \\ Some of studies using the terminal sliding mode control which enables a time-fixed process with the flight prescribed a priori \cite{Lian2013}; a fixed-time glideslope guidance algorithm on a quasi-periodic halo orbit \cite{Lian2012}.\\ This report presents the SDRE technique for the rendezvous scenario formulated as a control with state constraints: the LOS cone has been designed as a constraint for the relative position and a weight function that encourages the chaser to follow the axis of the approaching cone. This new method was tested, by means of Monte Carlo simulation, on relative motion in Near Rectilinear orbit, a particular Halo orbit.

\newpage

\section{Phase of rendez-vous}
The purpose of this chapter is to give a short overview of the different phases of a rendezvous approach and to describe the major issues of these phases. A rendezvous mission can be divided in this simple phases: 
\begin{itemize}
	\item launch;
	\item phasing;
	\item far range rendezvous;
	\item close range rendezvous;
	\item mating.
\end{itemize}
 During these phases, the kinematic and dynamic conditions that will eventually allow the connection of the chaser to the target spacecraft are successively established.

\subsection{Launch phase}

Owing to the rotation of the Earth, each point on its surface passes twice per day through any orbit plane. Since at most launch sites have a limited sector of launch directions that can be used (e.g. toward the sea), and so there is only one opportunity per day to launch a spacecraft into a particular orbit plane. During every minute the launch site move $\approx{\SI{0.25}{deg}}$ w.r.t. the orbital plane so the size of the launch window, i.e. the margin around the time when the launch site passes through the orbital plane, will mainly be determined by the correction capabilities of the launcher. At the end of the launch phase, the chaser vehicle has been brought by the launcher  into a stable orbit in the target orbital plane. After separation from the launcher, the spacecraft has to deploy its solar arrays and antennas and must initialise all its subsystems.

\subsection{Phasing}

After separation from the launcher the chaser vehicle is on a lower orbit and may be at an arbitrary phase angle behind the target. The objective of this first orbital phase of a rendezvous mission is to reduce the phase angle between the chaser and target spacecraft, by making use of the fact that a lower orbit has a shorter orbital period.\\ Phasing ends with the acquisition of either an "initial aim point'', or with the achievement of a set of margins for position and velocity values at a certain range, called the "trajectory gate'' or "entry gate''. The margins of the "aim point'' or the "gate'' must be achieved to make the final part of the approach possible. The aim point or ``gate'' will be on the target orbit, or very close to it, and from this position the far range relative rendezvous operations can commence.

\subsection{Far range Rendez-vous}

In many publications this phase is called ``homing'', by analogy to the navigation term used for aircraft when approaching an airport. The major objective of the far range rendezvous phase is the reduction of trajectory dispersions, i.e. the achievement of position, velocity and angular rate conditions which are necessary for the initiation of the close range rendezvous operations. Major tasks of this phase are the acquisition of the target orbit, the reduction of approach velocity and the synchronisation of the mission timeline. Far range rendezvous can start when relative navigation between chaser and target is available. The end point of this phase is usually a point from which standard rendezvous operations on standard trajectories at a fixed timeline can commence, a feature which is particularly desirable for an automatic rendezvous process.

\subsection{Close range Rendez-vous}
The close range rendezvous phase is usually divided into two subphases:
\begin{itemize}
	\item preparatory phase leading to the final approach corridor, often called "closing'';
	\item final approach phase leading to the mating conditions.
\end{itemize}

There are, of course, cases where no distinction can be made between a closing and a final approach subphase. This may be the case, e.g., for a V-bar approach, where the direction of motion remains the same and where no change of sensor type occurs.\\ The proximity to the target makes all operations safety-critical, requiring particular safety features for trajectory and onboard system design and continuous monitoring and interaction possibility by operators on ground and in the target station.

\subsubsection{Closing phase}
The objectives of the closing phase are the reduction of the range to the target and the achievement of conditions allowing the acquisition of the final approach corridor. This means that at the end of this phase the chaser is, concerning position, velocities, attitude and angular rates, ready to start the final approach on the proper approach axis within the constraints of the safety corridor.\\ In this case, toward the end of the closing phase, the acquisition conditions for the new sensor type have to be met. The rule of thumb is that the measurement accuracy must be of the order of 1\% of range or better.

\subsubsection{Final approach phase}
In this phase the objective is to achieve docking or berthing capture conditions in terms of positions and velocities and of relative attitude and angular rates. The attempted end condition is the delivery of chaser docking or capture interfaces into the reception range of the target docking mechanism or of the capture tool of the manipulator in the case of berthing.

\subsection{Maiting}
The mating phase starts when the GNC system of the chaser has delivered the capture interfaces of the chaser into the reception range of those of the target vehicle. This must be achieved within the constraints of the interface conditions, concerning:
\begin{itemize}
	\item for docking: 
		\begin{itemize}
  			\item[-] approach velocity;
			\item[-] lateral alignment;
			\item[-] angular alignment;
			\item[-] lateral and angular rates;
		\end{itemize}
	\item for berthing:
		\begin{itemize}
    			\item[-] position accuracy;
			\item[-] attitude accuracy;
			\item[-] residual linear rate;
			\item[-] angular rate.
		\end{itemize}
\end{itemize}

More of this paraghaph is ispired by \cite{Fehse2003}.

\begin{figure}[h]
  \includegraphics[scale = 0.7]{images/mission_points}
  \caption{\textit{This figure shows the rendez-vous phases. The labels are the nomenclature used for all hold and intermediate way points. The shaded area is the Keep Out Zone, which is defined for safety reasons.}}
  \label{fig:phases_point_rendez}
\end{figure}

\newpage


\end{document}
