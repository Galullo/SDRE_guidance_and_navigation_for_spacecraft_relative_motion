\documentclass[../main.tex]{thesis_style}
\graphicspath{{images/}{../images/}}

\begin{document}
\part{Simulation Results}
\newpage

\section{Relative Motion Control around Earth}
This section presents the preliminary part of project. Here we investigate the feasibility of SDRE algorithm in presence of constraints in rendezvous scenario, around the Earth. Here we used the assumption of Two-Body Problem.
The parametrization proposed in the previous sections, \cref{eq:SDC_NERM_2BP},  were used to develop SDRE controllers. Their performance were compared setting up a terminal rendezvous mission scenario. Simulations were developed in Simulink. Dorman-Price integration algorithm was used. SDRE controllers weight matrices coefficients were tuned in order to achieve a rendezvous condition in an acceptable time and passable propellent consumption.\\
\emph{Internationale State Space} (ISS) orbit was chosen as target' s orbit with the following orbital elements \cite{NASAISS}:

\begin{equation*}
\begin{matrix}
a  = 6.7644 \cdot 10^3 \quad km; & e = 0.00051; & i = 51.6391^{\circ};\\ \Omega = 270.8069^{\circ}; & \omega = 116.0974^{\circ}; & f(t_0) = 0^{\circ};
\end{matrix}
\end{equation*}

\begin{figure}[h]
	\centering
	\includegraphics[scale = 0.4]{orbit1.eps}
	\caption{\textit{International Space Station orbit.}}
\end{figure}

\subsection{Guidance and Navigation}
The position is assumed as available measurement, so $\mathbf{H}(\mathbf{x}) = \begin{bmatrix} \mathbf{I}_{3 \times 3} \quad \boldsymbol{0}_{3 \times 3} \end{bmatrix}$. The error that affect the measuremets is considered as purely random with a Gaussian distribution and standard deviation $\sigma = 1/3 \times 10^{-2}$ $m$ \cite{Massari2012}. \\ In this subsection the SDRE filter and control were tested in the same way as before. 
The control weight matrices were set as follow:
\newline
\begin{equation}
\mathbf{Q}_0 = \begin{bmatrix} 10^{-3} \cdot \mathbf{I}_{3 \times 3} & \boldsymbol{0}_{3 \times 3} \\ \boldsymbol{0}_{3 \times 3} & 10^{2} \cdot \mathbf{I}_{3 \times 3} \end{bmatrix} \qquad \mathbf{R}_0 = 10^{5} \cdot \mathbf{I}_{3 \times 3}
\end{equation}
\newline
\\Also the constraint matrix is set as follow, \cref{sec:weight_function}:\\
\newline
\begin{equation}
  \mathbf{W}_{z} = \begin{bmatrix} 50 \cdot \mathbf{f}_{cone}(\mathbf{x}) & 0 & 0 \\ 0 & 50 \cdot \mathbf{f}_{cone}(\mathbf{x}) &  0 \\ 0 & 0 & 5 \cdot \mathbf{f}_{axis}(\mathbf{x}) \end{bmatrix}
\end{equation}

The measurements noise covariance matrix is set as:
\begin{equation*}
\mathbf{R}_f = 10 ^{-5} \cdot \mathbf{I}_{3 \times 3}
\end{equation*}

the process noise covariance was chosen as:
\begin{equation*}
\mathbf{Q}_f = \begin{bmatrix} \boldsymbol{0}_{3 \times 3} & \boldsymbol{0}_{3 \times 3} \\ \boldsymbol{0}_{3 \times 3} & 10^{-12} \cdot \mathbf{I}_{3 \times 3} \end{bmatrix} 
\end{equation*}

the initial condition for error covariance matrix is:
\begin{equation*}
\mathbf{P}_f^0 = \begin{bmatrix}10^{-4} \cdot \mathbf{I}_{3 \times 3}  & \boldsymbol{0}_{3 \times 3} \\ \boldsymbol{0}_{3 \times 3} & 10^{-5} \cdot \mathbf{I}_{3 \times 3} \end{bmatrix} 
\end{equation*}

the filter start from state:
\begin{equation*}
\mathbf{x}_f^0 = \mathbf{x}^0 + \begin{bmatrix} \xi_p \\ \xi_v \end{bmatrix}
\end{equation*}
where $\mathbf{x}^0$ is the real relative position and velocity of chaser,  $\xi_p$ and  $\xi_v$ are $3 \times 1$ vectors of uniformly distributed random number, rispectively in the interval (0,$10^{-4}$) [$km$] and (0,$10^{-5}$) [$km/s$]. \\
Simulations results for this first set of tests are shown \cref{fig:tests_guidance_and_navigation_2BP}.
\begin{figure}[h]
	\centering
	\includegraphics[scale=0.2]{mc_test2BP_guidance_navigation}
	\caption{\textit{Simulation with 20 points for 5, 8, 11, 14, 17, 20 $km$ of relative distance between target and chaser.}}
	\label{fig:tests_guidance_and_navigation_2BP}
\end{figure}
 
\newpage
\begin{figure}[h]
	\begin{minipage}[t]{7cm}
		\centering
		\includegraphics[scale=0.18]{time_of_flight_2BP_guidance_navigation}
		\caption{\textit{The average time needed to reach the rendezvous conditions around Earth.}}   
	\end{minipage} 
	\hspace{3mm}
  	\begin{minipage}[t]{7cm}
    		\centering
    		\includegraphics[scale = 0.18]{cui_2BP_guidance_navigation}
		\caption{\textit{Propellant consumption is the integral over time of $||u||$ for rendezvous around Earth.} }    
    	\end{minipage} 
	 \label{fig:tests_guidance_and_navigation_2BP}
\end{figure}

Our goal is to determine the feasibility of algorithm and the results show a good behaviour in our region of interest: Line-of-Sight cone while approaching the docking port on target platform. The time of flight is also comparable with the time of flight of real mission \cite{Fehse2003}, so we investigated the feasibility of this algorithm in another rendezvous scenario: rendezvous around Moon in Near-Rectilinear Orbit where the relative motion is influenced by both Earth and Moon gravitational fields, see \cref{sec:rel_mot_3BP}.

\newpage
\section{Motion Control in Earth-Moon system}
Due to the nonlinearity of the gravitational acceleration and the presence of several time-varying parameters, the equations of relative motions may be difficult to use, specially in the navigation system where the filter needs a lot of information such as: position, velocity  acceleration from target, and position, velocity, angular velocity, angular acceleration, angular jerk of Moon. Under the assumption of primaries revolving in circular orbits, the number of time-varying parameters reduces and the filter requires only position and velocity from target \cite{Franzini2017}. In terms of maneuver time and fuel consumption the comparison between elliptical  and circular assumption model was done. In this work we suppose that the rendezvous maneuvers take place close \emph{periselene}, the point in orbit closest to the Moon.\\ The parametrization expressed in \cref{eq:SDC_NERM_moon} was used in SDRE controllers. Simulations were developed in Simulink. Dorman-Price integration algorithm was used. The guidance and navigation system run at 1 $Hz$. The distances were normalized in units of Moon orbit semi-major axis $a$, time in units of the inverse of mean angular motion $n$ and the masses such that $M_e + M_m = 1$ \cite{Koon2011}. 

\subsection{Guidance} \label{sec:Guidance_3BP_sim}
The SDRE controller was tested by means of Montecarlo simulation for six different $\rho = \begin{Bmatrix} 5, 8, 11, 14, 17, 20 \end{Bmatrix}$ [$km$]. For each $\rho$ is chosen 20 random uniformly distributed points. The weight matrices coefficients used are:\\
\begin{equation}
\mathbf{Q}_0 = \begin{bmatrix}  \mathbf{Q}_p & \boldsymbol{0}_{3 \times 3} \\ \boldsymbol{0}_{3 \times 3} & \mathbf{I}_{3 \times 3} \end{bmatrix} \qquad \mathbf{Q}_p = 4 \cdot \begin{bmatrix} 10^{5} & 0 & 0 \\ 0 & 10^{6} & 0 \\ 0 & 0 & 10^{5} \end{bmatrix}
\end{equation}
\begin{equation}
\mathbf{R}_0 = 5 \cdot 10^{-8} \cdot \mathbf{I}_{3 \times 3}
\label{eq:control_weight_matrices}
\end{equation}
\newline
\\and the constraint matrix is set as follow:\\
\newline
\begin{equation}
  \mathbf{W}_{z} = \begin{bmatrix} 20 \cdot \mathbf{f}_{cone}(\mathbf{x}) & 0 & 0 \\ 0 & 20 \cdot \mathbf{f}_{cone}(\mathbf{x}) &  0 \\ 0 & 0 & 10^{6} \cdot \mathbf{f}_{axis}(\mathbf{x}) \end{bmatrix}
\end{equation}

The terminal condition are $\rho \le 1$ $m$ (relative distance) and $\dot{\rho} \le 0.03$ $m/s$ (relative velocity). Note that ESA's ATV docking, the deputy had to converge and to keep the following conditions: $\rho < 20$ $m$, $\dot \rho  < 0.01$ $m/s$ \cite{Fehse2003}.\\ Simulations results for elliptical case is shown in \cref{fig:test_feasibility_3BP}, \cref{fig:trend_tof_3BP},\cref{fig:trend_cui_3BP}. 
\newpage
\begin{figure}[h]
	\centering
    	\includegraphics[scale=0.2]{mc_test3BP_guidance} 
    	\caption{\textit{Simulation with 20 points for 5, 8, 11, 14, 17, 20 $km$ of relative distance between target and chaser.}}
    	\label{fig:test_feasibility_3BP}
\end{figure}
 
In the \cref{fig:test_feasibility_3BP} we can see how the chaser moves close to the target. In the simulation for $\rho =  17$ $km$, \cref{fig:particular} it is more evident how the controller moves the chaser in the corridor of approach, avoiding keep-out zone that surrounds the target.
\begin{figure}[h]
	\centering
    	\includegraphics[scale = 0.2]{images/mc_test3BP_guidance_particular}
	\caption{\textit{Simulation with 20 points for 17, 20 $km$ of relative distance between target and chaser.}}
	\label{fig:particular}
\end{figure}
%\newpage
\begin{center}
\begin{figure}[h]
	\centering
	\includegraphics[scale=0.2]{time_of_flight_3BP_guidance} 
	\caption{\textit{The average time needed to reach the rendezvous conditions.}}
	\label{fig:trend_tof_3BP}
\end{figure}
\hspace{0mm}
\begin{figure}
	\centering
    	\includegraphics[scale = 0.2]{cui_3BP_guidance}
    	\caption{\textit{Propellant consumption is the integral over time of $||u||$.} }
    	\label{fig:trend_cui_3BP}
\end{figure}
\end{center}
%\newpage
As we can see in \cref{fig:trend_tof_3BP} and \cref{fig:trend_cui_3BP}, the Time of Flight ($t_{of}$) and the \emph{total Control Usage Index} ($\delta_v$) increase with increasing the relative distance between chaser and target as we expected. Another important aspect is the standard deviation on $t_{of}$: when the chaser initial condition isn't in LOS cone, and the relative distance is less than 15 km the chaser moves very slow as long as reaches the approach corridor. It needs more time for moving close target. However the standard deviation can be reduced if increase the number of test for each given distance. Analogous consideration can be done for $\delta_v$ standard deviation.\\
The same test is done  with Circular Restricted 3 Body problem (CR3BP) assumption. The results are very similar to elliptical case as can be seen from \cref{tab:comparison_index}, so the circular case was chosen for the controller and filter design.
\newpage
\begin{table}[h]
\begin{center}
  \begin{tabular}{ c c c c c c c c } 
    \hline \hline
    $\rho$ [$km$] &  &5  & 8& 11& 14& 17& 20 \\ \hline
    $t_{of}^c - t_{of}^e$  [$min$] & $10^{-13}$ &0 &0 &0 &0 & 0.2842  & 0   \\ \hline
     $\delta_v^c - \delta_v^e$ [$m/s$] & $10^{-5}$  &0.3908  &0.3018 &0.2264 &0.0980 &0.5358 &0.5853 \\
    \hline \hline 
     \end{tabular}
     \captionof{table}{\textit{Flight of Time mean difference and Control Usage Index mean difference between circular and elliptical case.}}
 \label{tab:comparison_index} \end{center}
\end{table}
 
In the \cref{fig:test_outvel_CNERM} we can see the behaviour of the guidance system when the initial velocities of chaser point outward of the approach cone. The flight of time and fuel consumption are similar to the previous case.
\begin{figure}[h]
 \centering
    \includegraphics[scale=0.22]{mc_outvel_CNERM} 
    \caption{\textit{Simulation with 20 points for 5, 8, 11, 14, 17, 20 $km$ of relative distance between target and chaser with relative velocities that point outward of the approach cone.}}
    \label{fig:test_outvel_CNERM}
 \end{figure}
 
\subsection{Guidance and Navigation} \label{sec:Guidance_Navigation_CNERM_sim}
The position is assumed as available measurement, so $\mathbf{H}(\mathbf{x}) = \begin{bmatrix} \mathbf{I}_{3 \times 3} \quad \boldsymbol{0}_{3 \times 3} \end{bmatrix}$. The error that affect the measurements is considered as purely random with Gaussian distribution, 0 mean and standard deviation $\sigma = 1/3 \times 10^{-2}$ $m$ \cite{Massari2012}. \\ In this subsection the SDRE filter and control were tested in the same way as before. 
The control weight matrices were set as in \cref{sec:Guidance_3BP_sim}. The process noise covariance and the measurements noise covariance matrix were set as:\\
\begin{equation*}
\mathbf{Q}_f = \begin{bmatrix} \boldsymbol{0}_{3 \times 3} & \boldsymbol{0}_{3 \times 3} \\ \boldsymbol{0}_{3 \times 3} & 10^{-12} \cdot \mathbf{I}_{3 \times 3} \end{bmatrix} \quad \mathbf{R}_f =  0.6768 \cdot 10 ^{-21} \cdot \mathbf{I}_{3 \times 3}
\end{equation*}

the initial condition for error covariance matrix is:\\
\begin{equation*}
\mathbf{P}_f^0 = \begin{bmatrix} 2.6015 \cdot 10^{-10} \cdot \mathbf{I}_{3 \times 3}  & \boldsymbol{0}_{3 \times 3} \\ \boldsymbol{0}_{3 \times 3} & 0.9820 \cdot10^{-5} \cdot \mathbf{I}_{3 \times 3} \end{bmatrix} 
\end{equation*}

the initial condition of filter is:\\
\begin{equation*}
\mathbf{x}_f^0 = \mathbf{x}^0 + \begin{bmatrix} \xi_p \\ \xi_v \end{bmatrix}
\end{equation*}
where $\mathbf{x}^0$ is the real relative position and velocity of chaser,  $\xi_p$ and  $\xi_v$ are $3 \times 1$ vectors of uniformly distributed random number, respectively in the interval (0, 10) [$cm$] and (0, 1) [$cm/s$]. \\ Simulations results are shown in \cref{fig:mc_CNERM_guidance_navigation}.
\begin{figure}[h]
 \centering
    \includegraphics[scale=0.24]{mc_CNERM_guidance_navigation} 
    \caption{\textit{Simulation with 20 points for 5, 8, 11, 14, 17, 20 $km$ of relative distance between target and chaser with assumption of CR3BP for Guidance and Navigation system.}}
    \label{fig:mc_CNERM_guidance_navigation}
 \end{figure}
\vspace{5mm}
 \begin{table}[h]
\begin{center}
  \begin{tabular}{c c c c c c c c} 
    \hline \hline
   $\boldsymbol{\rho}$ [$km$] &  &5 & 8& 11& 14& 17& 20 \\ \hline
    $Ie_\rho$ &  &5.0322& 8.0935& 11.1210& 14.2286& 17.6183& 20.5973   \\ \hline
    $Ie_{\dot{\rho}} $ &  &0.0158& 0.0202& 0.0194& 0.0210& 0.0245& 0.0240 \\
    \hline \hline 
     \end{tabular}
     \captionof{table}{\textit{Error index evaluated on position and velocity.}}
 \label{tab:err_index} \end{center}
\end{table}
 
 \newpage
The indexes were evaluated as follow:
\begin{equation*}
Ie_\rho = \int_{0}^{t_{of}} ||\mathbf{e}_\rho(t) || \, dt  \qquad Ie_{\dot{\rho}} = \int_{0}^{t_{of}} ||\mathbf{e}_{\dot{\rho}}(t)|| \, dt
\end{equation*}

where $\mathbf{e}_\rho(t)$ and $\mathbf{e}_{\dot{\rho}}(t)$ are the error vectors  respectively between real position and estimated position and real velocity and estimated velocity.

\begin{center}
\begin{figure}[h]
	\centering
	\includegraphics[scale=0.19]{time_of_flight_CNERM_guidance_navigation} 
	\caption{\textit{The average time needed to reach the rendezvous conditions with assumption of CR3BP for Guidance and Navigation system.}}
	\label{fig:trend_tof_CNERM}
\end{figure}
\hspace{0mm}
\begin{figure}[h]
	\centering
	\includegraphics[scale = 0.19]{cui_CNERM_guidance_navigation}
	\caption{\textit{Propellant consumption is the integral over time of $||u||$ with assumption of CR3BP for Guidance and Navigation system.} }
	\label{fig:trend_cui_CNERM}
\end{figure}
\end{center}
\newpage
\section{Conclusion}
This thesis work present a guidance and navigation system based on SDRE technique with state constraint in rendezvous missions with cis-lunar space station. Constraints and weight functions parameters were maintained as generic as possible. One possible SDC parametrization for relative motion in Earth-Moon system was found. Comparison between Elliptical and Circular assumption for relative motion equations, evaluated on time of flight and fuel consumption, was done. The results show that the performances are very close, so the Circular assumption was used to design the guidance and navigation system, taking the advantage that navigation system need only position and velocity measurements from target. Simulations demonstrate feasibility of the proposed control.

\end{document}
