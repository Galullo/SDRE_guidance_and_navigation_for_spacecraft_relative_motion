\documentclass[../main.tex]{thesis_style}
\graphicspath{{images/}{../images/}}

\begin{document}
\part{Essential Celestial Mechanics}
%\section{Introduction}
%-----------------------------------------------------------------
\newpage
\section{Two-Body Problem}\label{sec:2BP}
The \emph{Two-Body Problem} is the dynamic problem of two point masses under the influence of their mutual gravitational attraction according to Newton's Law.
Consider two particles of masses $m_1$ and $m_2$ with position vectors $\mathbf{R}_1$ and $\mathbf{R}_2$, as you can see in \cref{fig:2BP}, the equations of motion for the two bodies can be written using the Newton's Second Law:
\begin{equation}
  \begin{aligned}
    m_1 \begin{bmatrix}\ddot{\mathbf{R}}_1\end{bmatrix}_{\mathcal{I}} &= G \frac{m_1 m_2}{r^3}\mathbf{r}\\
    m_2 \begin{bmatrix} \ddot{\mathbf{R}}_2\end{bmatrix}_{\mathcal{I}} &= -G \frac{m_1 m_2}{r^3}\mathbf{r} 
  \end{aligned}
\end{equation}

where $G$ is the universal gravitational constant and $\mathbf{r} = \mathbf{R}_2 - \mathbf{R}_1$. So the relative acceleration of two bodies is

\begin{equation}
  \begin{bmatrix}\ddot{\mathbf{r}}\end{bmatrix}_{\mathcal{I}} = \begin{bmatrix}\ddot{\mathbf{R}}_2\end{bmatrix}_{\mathcal{I}} - \begin{bmatrix}\ddot{\mathbf{R}}_1\end{bmatrix}_{\mathcal{I}} =  -G \frac{m_1 + m_2}{r^3}\mathbf{r} \label{eq:2BP}
\end{equation}

In general, when we study the motion of satellite (or spacecraft) its mass is neglegible so the equation \cref{eq:2BP} begins:
\begin{equation}
  \begin{bmatrix}\ddot{\mathbf{r}}\end{bmatrix}_{\mathcal{I}} = -\mu_p \frac{\mathbf{r}}{r^3} \label{eq:2BP_sat}
\end{equation}

The \emph{gravitational parameter} $\mu_p = GM$ is introduced and $M$ is the mass of primary.\\
The \cref{eq:2BP_sat} describe the motion of small body (satellite or spacecraft) around a primary body (planet). This type of motion is usually called \emph{Keplerian Motion}.  

\begin{figure}[h]
  \centering
  \includegraphics[scale = 0.35]{2BP}
  \caption{\textit{Two Body Problem}.} \label{fig:2BP}
\end{figure}

The Keplerian motion takes place on a plane, as matter of fact the angular momentum of small mass is constant:

\begin{equation*}
\mathbf{h} = \mathbf{r} \times \mathbf{v} = \mathbf{r} \times \frac{d}{dt}\mathbf{r} = \frac{d}{dt}\left ( \mathbf{r} \times \mathbf{r} \right ) = \boldsymbol{0}
\end{equation*}

The solution of \cref{eq:2BP_sat} can be rapresented by the conic polar equation
\begin{equation}
r = \frac{p}{1 + e \cos f} \label{eq:2BP_sat_solution} 
\end{equation}

where $e$ is the \emph{eccentricity}, $p$ is the \emph{semilatus rectum} ($p = \frac{h^2}{\mu_p}$) and $f$ is the \emph{true anomaly}. The \emph{eccentricity} define the orbit shape:
\begin{itemize}
\item $e = 0$  circumference;
\item $0 < e < 1$ ellipse;
\item $e = 1$  parabola;
\item $e > 1$ hyperbola
\end{itemize}

The primary body occupies one of the conic focus. The closest point on the ellipse to the primary focus is called \emph{periapse}, whereas the furthest point is the \emph{apoapse}. \\
Other important quantities are the orbital period $$T = 2 \pi \sqrt{\frac{a^3}{\mu_p}}$$ ($a$ is the conic \emph{semi-major axis}) and the mean motion $$ n = \frac{2 \pi}{T} = \sqrt{\frac{\mu_p}{a^3}}$$
The true anomaly is measured from periapse and its time derivative can be computed as $$\dot f = \frac{h}{r^2} $$

\begin{figure}[h]
  \centering
  \includegraphics[scale = 0.3]{ellipse_geometry}
  \caption{\textit{Ellipse Geometry}.} \label{fig:ellipse_geometry}
\end{figure}


\newpage
\section{Three-Body Problem}\label{sec:3BP}
In general, the classical \emph{Three-Body Problem} regard the motion of infinitesimal body, $m_3$, in the gravity field of two primary massive bodies, $m_{1}$ and $m_{2}$. Suppose the positions of the three masses are located through the vectors $\mathbf{R}_1$, $\mathbf{R}_3$ and $\mathbf{R}_3$ relative to their center of mass (\cref{fig:3BP}), and assume the external forces are neglected, we see that the motion of the center of mass is a constant velocity straight-line, so we can adopt the center of mass as an inertial origin, then the three equations of motion are given by:

\begin{equation}
  \begin{aligned}
    m_1 \begin{bmatrix}\ddot{\mathbf{R}}_1 \end{bmatrix}_{\mathcal{I}} &= G\frac{m_1 m_2}{r_{12}^3}\mathbf{r}_{12} + G\frac{m_1 m_3}{r_{13}^3}\mathbf{r}_{13}  \\
    m_2 \begin{bmatrix}\ddot{\mathbf{R}}_2 \end{bmatrix}_{\mathcal{I}} &= -G\frac{m_1 m_2}{r_{12}^3}\mathbf{r}_{12} + G\frac{m_2 m_3}{r_{23}^3}\mathbf{r}_{23} \\
    m_3 \begin{bmatrix}\ddot{\mathbf{R}}_3 \end{bmatrix}_{\mathcal{I}} &= -G\frac{m_1 m_3}{r_{13}^3}\mathbf{r}_{13} - G\frac{m_2 m_3}{r_{12}^3}\mathbf{r}_{23} 
  \end{aligned}
\end{equation}

where $\mathbf{r}_{13} = \mathbf{R}_{3} - \mathbf{R}_{1}$, $\mathbf{r}_{23} = \mathbf{R}_3 - \mathbf{R}_2$ and $\mathbf{r}_{12} = \mathbf{R}_2 - \mathbf{R}_1$ and $G$ is the universal gravitational constant. Relative positions norms are indicated with $r_{13}$, $r_{23}$ and $r_{12}$. The notation $\begin{bmatrix}\ddot{\mathbf{R}}_i \end{bmatrix}_\mathcal{I}$ denotes the acceleration of the body as seen from the inertial frame.

\begin{figure}[h]
  \centering
  \includegraphics[scale = 0.5]{R3BP.png}
  \caption{\textit{Restricted Three Body Problem}.} \label{fig:3BP}
\end{figure}
\newpage
The equation of motion of $m_3$ with respect to $m_1$ and $m_2$ are given by:

\begin{equation}
  \begin{aligned}
    \begin{bmatrix}\ddot{\mathbf{r}}_{13} \end{bmatrix}_{\mathcal{I}} = \begin{bmatrix}\ddot{\mathbf{R}}_3 \end{bmatrix}_{\mathcal{I}} - \begin{bmatrix}\ddot{\mathbf{R}}_1 \end{bmatrix}_{\mathcal{I}} &= -G\frac{(m_1 + m_3)}{r_{13}^3}\mathbf{r}_{13} - G m_2 \left ( \frac{\mathbf{r}_{23}}{r_{23}^3} + \frac{\mathbf{r}_{12}}{r_{12}^3} \right ) \\
    \begin{bmatrix}\ddot{\mathbf{r}}_{23} \end{bmatrix}_{\mathcal{I}}= \begin{bmatrix}\ddot{\mathbf{R}}_3 \end{bmatrix}_{\mathcal{I}} - \begin{bmatrix}\ddot{\mathbf{R}}_2 \end{bmatrix}_{\mathcal{I}} &= -G\frac{(m_2 + m_3)}{r_{23}^3}\mathbf{r}_{23} - G m_1 \left ( \frac{\mathbf{r}_{13}}{r_{13}^3} - \frac{\mathbf{r}_{12}}{r_{12}^3} \right ) \\
  \end{aligned} \label{eq:E3BP}
\end{equation}

If the mass $m_3$ is negligible with respect to the primaries masses, i.e. $m_3 \ll m_1$ and $m_3 \ll m_2$, we consider the \emph{restricted three-body problem}. Under this assumption the $m_3$ doesn't affect the motion of two primaries, so the \cref{eq:E3BP} becomes:

\begin{equation}
  \begin{aligned}
    \begin{bmatrix}\ddot{\mathbf{r}}_{13} \end{bmatrix}_{\mathcal{I}} &= -\mu_1\frac{\mathbf{r}_{13}}{r_{13}^3} - \mu_2 \left ( \frac{\mathbf{r}_{23}}{r_{23}^3} + \frac{\mathbf{r}_{12}}{r_{12}^3} \right ) \\
    \begin{bmatrix}\ddot{\mathbf{r}}_{23} \end{bmatrix}_{\mathcal{I}} &= -\mu_2\frac{\mathbf{r}_{23}}{r_{23}^3} - \mu_1 \left ( \frac{\mathbf{r}_{13}}{r_{13}^3} - \frac{\mathbf{r}_{12}}{r_{12}^3} \right ) \\
  \end{aligned} \label{eq:R3BP}
\end{equation} 

where $\mu_1 = G m_1$ and $\mu_2 = G m_2$ are the gravitational parameter for $m_1$ and $m_2$ rispectively.


%-----------------------------------------------------------------
%\newpage
\subsection{Elliptical Restricted Three-Body Problem} \label{sec:ER3BP}
If the motion of the two primary bodies is constrained to elliptic orbits about their barycenter, the \emph{Elliptic Restricted Three-Body Problem} (ER3BP), the distance between the two primary bodies varies periodically.\\

\begin{figure}[h]
  \centering
  \includegraphics[scale = 0.35]{CR3BP.png}
  \caption{\textit{Planar Restricted Three-Body Problem}.} \label{fig:R3BP}
\end{figure}

By the following choice of units, the equation of motion can be written in non-dimensional form
\begin{itemize}
\item the unit of mass is taken to be $m_1 + m_2$;
\item the unit of length is chosen to be the constant separation between $m_1$ and $m_2$;
\item the unit of time is chosen such that the orbital period of $m_1$ and $m_2$ about their center of mass is $2\pi$.
\end{itemize}

The universal constant of gravitation then becomes $G = 1$, the mean motion of the primaries is also $n = 1$ (or $\omega = 1$) and the mass parameter of the system is $$\mu = \frac{m_{2}}{m_{1}+m_{2}}$$
The position vector $\mathbf{R}_3$ can be expressed in non-dimensional component $$\mathbf{R}_3 = \widetilde{x} \boldsymbol{\vec{i}} + \widetilde{y} \boldsymbol{\vec{j}} + \widetilde{z} \boldsymbol{\vec{k}}$$
so the equations of motion, under assumption that the primaries revolve around their common barycenter in elliptic orbits are:\\
\begin{equation}
  \begin{cases}
    \stackrel{\circ\circ}{\widetilde{x}}- 2\widetilde{\omega}\stackrel{\circ}{\widetilde{y}} - \stackrel{\circ}{\widetilde{\omega}}\widetilde{y} -\widetilde{\omega}^{2}\widetilde{x} = -\mu \frac{\widetilde{x} + \widetilde{R}_2}{\widetilde{r}_{23}^3} - (1-\mu)\left (\frac{\widetilde{x} - \widetilde{R}_1}{\widetilde{r}_{13}^3} + \frac{1}{\widetilde{r}_{12}^2}\right) \\
    \stackrel{\circ\circ}{\widetilde{y}} + 2\widetilde{\omega}\stackrel{\circ}{\widetilde{x}} + \stackrel{\circ}{\widetilde{\omega}}\widetilde{x} -\widetilde{\omega}^{2}\widetilde{y} = -\mu \frac{\widetilde{y}}{\widetilde{r}_{23}^3}  - (1-\mu)\frac{\widetilde{y}}{\widetilde{r}_{13}^3}\\
    \stackrel{\circ\circ}{\widetilde{z}} = -\mu \frac{\widetilde{z}}{\widetilde{r}_{23}^3}  - (1-\mu)\frac{\widetilde{z}}{\widetilde{r}_{13}^3}
  \end{cases}
  \label{eq:ER3BP}
\end{equation}\\

where $\circ$ denote the time derivative in non-dimentional and $\widetilde{r}_{13}^2 = (\widetilde{x} - \widetilde{R}_1)^2 + \widetilde{y}^2 + \widetilde{z}^2$, $\widetilde{r}_{23}^2 = (\widetilde{x} + \widetilde{R}_2)^2 + \widetilde{y}^2 + \widetilde{z}^2$, as we can see in \cref{fig:R3BP}
\newline
\subsection{Circular Restricted Three-Body Problem} \label{sec:CR3BP}

This section considers the \emph{Circular Restricted Three-Body Problem} (CR3BP) in which the motion of the two primary bodies is constrained to circular orbits about their barycenter. The CR3BP was formulated in the first time by Euler (1772) for the Sun–Earth–Moon system to study the motion of the Moon about the Earth but perturbed by the Sun.
By means of the Lagrangian formulation, as given in \cite{Wie2}, we obtain the following set of equations:

\begin{equation}
  \begin{cases}
    \stackrel{\circ\circ}{\widetilde{x}}-2\stackrel{\circ}{\widetilde{y}} = \frac{\partial U(\widetilde{x},\widetilde{y},\widetilde{z})}{\partial \widetilde{x}}\\
    \stackrel{\circ\circ}{\widetilde{y}}+2\stackrel{\circ}{\widetilde{x}} = \frac{\partial U(\widetilde{x},\widetilde{y},\widetilde{z})}{\partial \widetilde{y}}\\
    \stackrel{\circ\circ}{\widetilde{z}} = \frac{\partial U(\widetilde{x},\widetilde{y},\widetilde{z})}{\partial \widetilde{z}}
  \end{cases}
  \label{eq:CR3BP}
\end{equation}\\

where $\circ$ denote the time derivative in non-dimentional variables and in the right hand of equations there are partial derivative of gravitational potential $$U(\widetilde{x},\widetilde{y},\widetilde{z}) = \frac{1}{2}(\widetilde{x}^2 + \widetilde{y}^2) + \frac{1-\mu}{\widetilde{r}_{13}} + \frac{\mu}{\widetilde{r}_{23}}$$
and $$\widetilde{r}_{13}^2 = (\widetilde{x} + \mu)^2 + \widetilde{y}^2 + \widetilde{z}^2$$ $$\widetilde{r}_{23}^2 = (\widetilde{x} -1 +\mu)^2 + \widetilde{y}^2 + \widetilde{z}^2$$

Note that \cref{eq:ER3BP} becames \cref{eq:CR3BP} if $\widetilde{\omega} = 1$ and $\stackrel{\circ}{\widetilde{\omega}} = 0$.

\subsubsection{Equilibrium or Lagrangian Points} \label{Lagrangian Point}

Setting all derivatives in \cref{eq:CR3BP} to zero, we can find equilibrium points of the $m_{1}$-$m_{2}$ system. In every equilibrium points, the gravitational forces and the centrifugal force acting on the $m_3$ are balanced. Five equilibrium points of the \emph{Circular Restricted Three-Body Problem} exist, as illustrated in \cref{fig:Lagrangian_points}.The first three are on the line connecting the two large bodies and the last two, $L_{4}$ and $L_{5}$, each form an equilateral triangle with the two large bodies. The $L_{1}$, $L_{2}$, and $L_{3}$ points are nominally unstable. In \cite[38]{Koon2011} explain how compute the $x$ values, $\gamma_{i}$, of the collinear points.\\
Although the Lagrange point is just a point in empty space, its peculiar characteristic is that it can be orbited and we can find more kind of orbit.

\begin{figure}[h]
  \centering
  \includegraphics[scale = 0.3]{Lagrangian_points.png}
  \caption{\textit{Lagrangian Points in Circular Restricted Three-Body Problem}.} \label{fig:Lagrangian_points}
\end{figure}

The same considerations and the same resutls can get for ER3BP.
\newpage
\section{Orbit around collinear point}

Robert Farquhar \cite{Farquhar1973} discovered trajectories around $L_{2}$ in the Earth-Moons system. He named such a trajectory a \emph{halo orbit} as it appeared from the Earth to be a halo encircling the Moon. Halo orbits are the result of a complicated interaction between the gravitational pull of the two planetary bodies and the Coriolis and centrifugal accelerations on object with mass $m_3$.\\
Farquhar used analytical expressions to represent halo orbits, but Kathleen Howell \cite{Howell1984_2} showed that more precise trajectories could be computed numerically.\\
The studies on the orbits near libration point discovered other particular kind of orbit:

\begin{itemize}
\item planar and vertical families of Lyapunov periodic orbits;
\item three-dimensional quasi-periodic Lissajous orbits;
\item periodic halo orbits;
\item quasi-halo orbits.
\end{itemize}

\subsection{Halo Orbit}

The first halo orbit mission ISEE-3 was designed using methodology described by Richardson \cite{Richardson1980}.\\ The halo orbit was constructed using the third-order analytical solution obtained by an application of successive approximations using the Lindstedt–Poincar'e method.\\
Some computational advantages can be obtained if the \cref{eq:CR3BP} are developed using Legendre polynomials $P_{n}$ to expand the nonlinear terms $\frac{1-\mu}{r_{13}} + \frac{\mu}{r_{23}}$ in the gravitational potential. The following formula is used:

\begin{equation}
\frac{1}{\sqrt{(\widetilde{x}-A)^{2}+(\widetilde{y}-B)^{2}+(\widetilde{z}-C)^{2}}}=\frac{1}{D}\sum_{n=0}^\infty \left(\frac{\rho}{D}\right)^n P_{n}\left(\frac{A\widetilde{x}+B\widetilde{y}+C\widetilde{z}}{D\rho}\right)
\end{equation}\\

where $D^2 = A^{2}+B^{2}+C^{2}$ and $\rho^2 = \widetilde{x}^{2}+\widetilde{y}^{2}+\widetilde{z}^{2}$.\\After that we can write the \cref{eq:CR3BP} as:

\begin{equation}
  \begin{cases}
    \stackrel{\circ \circ}{\widetilde{x}}-2\stackrel{\circ}{\widetilde{y}}-(1+2c_{2})\widetilde{x} = \frac{\partial}{\partial \widetilde{x}} \sum_{n\ge 3}c_{n}\rho^{n}P_{n}\left(\frac{\widetilde{x}}{\rho}\right) \\ \\
    \stackrel{\circ \circ}{\widetilde{y}}+2\stackrel{\circ}{\widetilde{x}}+(c_{2}-1)\widetilde{y}= \frac{\partial}{\partial \widetilde{y}} \sum_{n\ge 3}c_{n}\rho^{n}P_{n}\left(\frac{\widetilde{y}}{\rho}\right)\\ \\
    \stackrel{\circ \circ}{\widetilde{z}}+c_{2}\widetilde{z} = \frac{\partial}{\partial \widetilde{z}} \sum_{n\ge 3}c_{n}\rho^{n}P_{n}\left(\frac{\widetilde{z}}{\rho}\right)
  \end{cases}
  \label{eq:motion_l}
\end{equation}\\

you can see in \cite[146]{Koon2011} how compute the $c_{n}(\mu,\gamma)$ coefficients.

\subsubsection{Linearized Motion}

Considering the linear part of \cref{eq:motion_l}:

\begin{equation}
  \begin{cases}
        \stackrel{\circ \circ}{\widetilde{x}}-2\stackrel{\circ}{\widetilde{y}}-(1+2c_{2})\widetilde{x} = 0 \\
    \stackrel{\circ \circ}{\widetilde{y}}+2\stackrel{\circ}{\widetilde{x}}+(c_{2}-1)\widetilde{y} = 0\\
    \stackrel{\circ \circ}{\widetilde{z}}+c_{2}\widetilde{z} = 0
  \end{cases}
  \label{eq:motion_l_lin}
\end{equation}\\

we can be deduced the existence of periodic solution. As we know, the solution of the characteristic equation of \cref{eq:motion_l_lin} has two real and four imaginary roots $(\pm\lambda, \pm i\omega_{p}, \pm i\omega_{v})$. If we choose the initial conditions adequately so that only the non-divergent mode is allowed, the $xy$-solution will be bounded. In this case, the linearized equations have solutions of the form

\begin{equation}
  \begin{cases}
    \widetilde{x} = -A_{x}\cos(\omega_{p}\widetilde{t} + \phi) \\
    \widetilde{y} = \kappa A_{x}sin(\omega_{p}\widetilde{t} + \phi) \\
    \widetilde{z} = A_{z}\sin(\omega_{v}\widetilde{t} + \psi)
  \end{cases}
  \label{eq:sol_motion_l_lin}
\end{equation}\\

where $\kappa = \frac{\omega_{p}^{2}+1+2c_{2}}{2\omega_{p}}$. Note that the linearized motion will become quasi-periodic if the in-plane and out-of-plane frequencies are such that their ratio is irrational.\\
If the amplitudes $A_{x}$ and $A_{z}$ are constrained by a certain non-linear algebraic relationship:

\begin{equation}
  l_{1}A_{x}^{2}+l_{2}A_{z}^{2}+\Delta = 0
  \label{cond:amplitude_halo}
\end{equation}

that it was found as result of the applicaiton of the perturbation method, the eigenfrequencies of the \cref{eq:motion_l_lin} are equal (for $\Delta$, $l_{1}$, $l_{2}$ see \cite[156]{Koon2011}, \cite{Richardson1980}).\\

Another importat condition there is between the phases $\phi$ and $\psi$ that are related in linear fashion:

\begin{equation}
  \psi-\phi = p\frac{\pi}{2},  \qquad p = 1, 3
  \label{cond:phase_halo}
\end{equation}

Note that if $A_{x}>\sqrt[2]{\frac{\Delta}{l_{1}}}$  we have a bifurcation that manifests itself through the condition espressed in \cref{cond:phase_halo} so any halo orbit can be characterized completely by specifying a particular out-of-ecliptic plane amplitude $A_{z}$:

\begin{itemize}
\item $p = 1$ and $A_{z}$ is positive, we have the \emph{northern halo} whose maximum out-of-plane component is above the xy-plane;
\item $p = 3$ and $A_{z}$ is negative, we have the \emph{southern halo} whose maximum out-of-plane component is below the xy-plane.
\end{itemize}

Northern and southern halo orbits with the same $A_{z}$ amplitude are specular images across the xy-plane \cref{fig:halo_picture}.

\begin{figure}[h]
  \centering
  \includegraphics[scale = 0.4]{halo_picture.png}
  \caption{\textit{Northern halo orbit (above) and Southern halo orbit (below)}}\label{fig:halo_picture}
\end{figure}
\subsubsection{Third-Order Approximation}

Recall the \cref{eq:motion_l}, Richardson stopped the \emph{Legendre} polynomial at third-order so the equations of motion begin:

\begin{equation}
  \begin{cases}
        \stackrel{\circ \circ}{\widetilde{x}}-2\stackrel{\circ}{\widetilde{y}}-(1+2c_{2})\widetilde{x} =  \frac{3}{2}c_{3}(2\widetilde{x}^{2}-\widetilde{y}^{2}-\widetilde{z}^{2})+2c_{4}\widetilde{x}(2\widetilde{x}^{2}-3\widetilde{y}^{2}-3\widetilde{z}^{2}) + O(4) \\ \\
    \stackrel{\circ \circ}{\widetilde{y}}+2\stackrel{\circ}{\widetilde{x}}+(c_{2}-1)\widetilde{y} = -3c_{3}\widetilde{x}\widetilde{y}-\frac{3}{2}c_{4}\widetilde{y}(4\widetilde{x}^{2}-\widetilde{y}^{2}-\widetilde{z}^{2}) + O(4)\\ \\
    \stackrel{\circ \circ}{\widetilde{z}}+c_{2}\widetilde{z} = -3c_{3}\widetilde{x}\widetilde{z}-\frac{3}{2}c_{4}\widetilde{z}(4\widetilde{x}^{2}-\widetilde{y}^{2}-\widetilde{z}^{2}) + O(4)
  \end{cases}
  \label{eq:motion_l_ric3}
\end{equation}\\

By forcing the linearized $z$ equation with conditions in \cref{cond:amplitude_halo,cond:phase_halo} and replacing $c_{2}$ with $\omega_{p}$ in \cref{eq:motion_l_ric3}, so it becomes necessary to rewrite the left-hand side of the $z$ equation and to introduce a correction term  $\Delta = \omega_{p}^{2}-c_{2} = \omega_{p}^{2}-\omega_{v}^{2}$. In addition, to help remove secular terms, Richardson used a new independent variable $$\tau = \nu \widetilde{t},$$ where $$\nu = 1 + \sum_{n\ge 1}\nu_{n}, \qquad con \qquad \nu_{n}<1.$$ It is important to note that $\nu_{n}$ are assumed to be $O(A_{z}^{n})$, so $A_{z}\ll 1$ \cite[155]{Koon2011}.\\
This is the \emph{Lindstedt–Poincar'e Method} and the equations of motion are written in terms of new independent variable:\\

\begin{equation}
  \begin{cases}
   \nu^{2} \stackrel{\circ \circ}{\widetilde{x}}-2\nu\stackrel{\circ}{\widetilde{y}}-(1+2c_{2})\widetilde{x} = \frac{3}{2}c_{3}(2\widetilde{x}^{2}-\widetilde{y}^{2}-\widetilde{z}^{2})+2c_{4}\widetilde{x}(2\widetilde{x}^{2}-3\widetilde{y}^{2}-3\widetilde{z}^{2}) + O(4) \\ \\
   \nu^{2} \stackrel{\circ \circ}{\widetilde{y}}+2\nu\stackrel{\circ}{\widetilde{x}}+(c_{2}-1)\widetilde{y} = -3c_{3}\widetilde{x}\widetilde{y}-\frac{3}{2}c_{4}\widetilde{y}(4\widetilde{x}^{2}-\widetilde{y}^{2}-\widetilde{z}^{2}) + O(4)\\ \\
   \nu^{2} \stackrel{\circ \circ}{\widetilde{z}}+\omega_{p}^{2}\widetilde{z} = -3c_{3}\widetilde{x}\widetilde{z}-\frac{3}{2}c_{4}\widetilde{z}(4\widetilde{x}^{2}-\widetilde{y}^{2}-\widetilde{z}^{2})+\Delta \widetilde{z} + O(4)
  \end{cases}
  \label{eq:motion_l_ric3_p}
\end{equation}\\

Most of the secular terms can be removed by using that:
\begin{equation}
  \nu_{1} = 0, \qquad \nu_{2} = s_{1}A_{x}^{2}+s_{2}A_{z}^{2}
  \label{cond:secular_terms}
\end{equation}\\

After that, the halo orbit period is $T = \frac{2\pi}{\omega_{p}\nu}$.

\subsection{Near Rectilinear Orbit}

Analytical approximations must be combined with numerical techniques to generate a halo orbit accurate enough. We need the $6\times 6$ \emph{State Transition Matrix} (STM), $\bm{\Phi}(\widetilde{t},0)$ of  $\frac{\partial \bm{X}(\widetilde{t})}{\partial \bm{X}(0)}$ where $$\bm{X} = \begin{bmatrix} \widetilde{x}\\ \widetilde{y}\\ \widetilde{z}\\ \stackrel{\circ}{\widetilde{x}}\\ \stackrel{\circ}{\widetilde{y}} \\ \stackrel{\circ}{\widetilde{z}} \end{bmatrix}$$ is the column vector of the states of \cref{eq:CR3BP}. We started from $\bm{\Phi}(0, 0) = \bm{I}_{6\times 6}$, and then we integrate simultaneously the following ODEs:

\begin{equation}
  \frac{d}{d\widetilde{t}}\bm{\Phi}(\widetilde{t},0) = \bm{A}(\widetilde{t})\bm{\Phi}(\widetilde{t},0)
  \label{eq:stm}
\end{equation}\\

with
$$\bm{A}(\widetilde{t})  = \left(\begin{matrix}  \bm{0}_{3 \times 3} & & \bm{I}_{3\times 3}\\ \\  \bm{U}_{XX} & & 2\bm{\Omega} \end{matrix}\right) \qquad \qquad \bm{\Omega} = \left(\begin{matrix} 0 & 1 & 0 \\ -1 & 0 & 0 \\ 0 & 0 & 0 \end{matrix}\right)$$\\
$\bm{U}_{XX}$ is the symmetric matrix of second partial derivatives of $U(\widetilde{x},\widetilde{y},\widetilde{z})$ with respect to $\widetilde{x},\widetilde{y},\widetilde{z}$, evaluated along the orbit. The stability of the orbit is determined by the eigenvalues of $\bm{\Phi}(T_{F}, 0)$, where $T_{F}$ is the period of orbit.Two of the eigenvalues are always 1 and the other four are in reciprocal pairs ($\lambda_{i}, \frac{1}{\lambda_{i}}$).\\
As we can see in \cite{Howell1984_1}, the point with the maximum $\widetilde{x}$ and maximum $\widetilde{z}$ value is unique for each orbit, so we can choose the $x_{max}$ value to identify each member of the family. It can be seen in \cref{fig:xz_halo_family,fig:yz_halo_family}, the family is tending toward orbits to have a rectilinear shape, which have a large out-of-plane component $z$ and increasingly smaller in-plane components.

\begin{figure}[h]
  \begin{minipage}[b]{7cm}
    \centering
    \includegraphics[scale=0.25]{xz_halo_orbits_L3_family.png} 
    \caption{\textit{Example of family of halo orbits in $L_{3}$ $\mathit{xz}$-plane}.}
    \label{fig:xz_halo_family}
  \end{minipage} 
  \hspace{3mm}
  \begin{minipage}[b]{7cm}
    \centering
    \includegraphics[scale = 0.25]{yz_halo_orbits_L3_family.png}
    \caption{\textit{Example of family of halo orbits in $L_{3}$ $\mathit{yz}$-plane}.}
    \label{fig:yz_halo_family}
    \end{minipage} 
\end{figure}

\subsubsection{Modelling as a Perturbated Keplerian Orbit}

When the orbits are very close to the larger mass (if we consider the $L_{3}$ point) or the smaller mass (if we consider the $L_{1}$ or $L_{2}$), the distance from $m_{i}$
is given by $\bm{r}$, and we can modell their as a \emph{perturbed Kepler orbits}.\\
Let the eccentricity tend to unity, the ellipse becomes more and more elongated, with the perihelion distance $a(1-e)$ tending to zero: this is called a \emph{rectilinear ellipse}. In the \cite[88]{Roy2005} we can find the following equation, that relating time, position and velocity when the motion is rectilinear as in the two-body problem:

\begin{align}
  &M = n(t-\tau)   \label{eq:rectilinear_mean_anomaly_1}   \\
  &M = E-sin(E)    \label{eq:rectilinear_mean_anomaly_2}   \\
  &r = a(1-cos(E)) \label{eq:rectilinear_ditance_r}        \\
  &r\dot r = a^{1/2}\mu^{1/2}sin(E) \label{eq:rectilinear_dif_relation}
\end{align}

They may also be used for \emph{near-rectilinear motion}. In the elliptic case when $e\approx 1$  Kepler’s equation begin:

\begin{equation}
  \begin{split}
    M &= E-e\cdot sin(E)\\
    &= E-sin(E)+(1-e)\cdot sin(E)\\
    &= E-sin(E)+\epsilon \cdot sin(E)
  \end{split}
\end{equation}

$\epsilon\approx0$, so we have the \cref{eq:rectilinear_mean_anomaly_2}.
After that, as we can see in \cite{Howell1984_2}, the presence of the farthermost mass of primary bodys not only disturb the position of $m_3$ from that in a 2-body Kepler orbit, it also make a time correction necessary.\\
The equation of perturbed  motion turn into:

\begin{equation}
  \bm{\ddot r} = -\frac{n^{2} a^{2}}{r^{2}}+\bm{f_{r}}
\end{equation}

\newpage
\section{Relative Motion}
\subsection{Relative Motion in Two-Body Problem}
The relative motion involves two or more spacecraft/bodies in orbit around a primary body: a target and a chaser in the case of a rendezvous mission; a chief, or formation leader, and one ore more deputies, or followers, if we are considering a formation.
\begin{figure}[h]
	\begin{minipage}[t]{7cm}
		\centering
		\includegraphics[scale = 0.3]{relative_motion_earth}
		\caption{\textit{Rendezvous scenario}.}
	\end{minipage}
	\begin{minipage}[t]{7cm}
		\centering
		\includegraphics[scale = 0.3]{LVLH_frame_earth}
		\caption{\textit{LVLH frame}.}
	\end{minipage}
\end{figure}

In the Cartesian formulation of relative motion dynamic is developed in the \emph{local - vertical local - horizon frame} (LVLH), centered on the target (or chief) center of mass. For two-Body problem, in this report, the unit vectors are given by:  
\begin{equation}
\hat{\mathbf{i}} = \hat{\mathbf{j}} \times \hat{\mathbf{k}},\qquad \hat{\mathbf{j}} = -\frac{\mathbf{h}_{t/e}}{h_{t/e}}, \qquad \hat{\mathbf{k}} = -\frac{\mathbf{r}_{t}}{r_{t}}
\label{eq:LVLH2}
\end{equation}
\begin{comment}
\begin{equation*}
\mathcal{L} = \begin{Bmatrix} \mathbf{R}_t; \hat{\mathbf{i}},\hat{\mathbf{j}}, \hat{\mathbf{k}} \end{Bmatrix}\quad =\quad \frac{\mathbf{r}}{r}, \quad \frac{\mathbf{h}}{h} \times \frac{\mathbf{r}}{r},\quad \frac{\mathbf{h}}{h}
\end{equation*}
\end{comment}
Let $ \boldsymbol{\rho} $ and $ \dot{\boldsymbol{\rho}} $ denote, respectively, chaser's relative position and velocity vectors
with respect to the target
\begin{equation*}
\boldsymbol{\rho} = x \hat{\mathbf{i}} + y \hat{\mathbf{j}} + z \hat{\mathbf{k}}  \qquad \dot{\boldsymbol{\rho}}= \dot{x} \hat{\mathbf{i}} + \dot{y} \hat{\mathbf{j}} + \dot{z} \hat{\mathbf{k}}
\end{equation*}

and $\boldsymbol{\omega}$ and $\dot{\boldsymbol{\omega}}$ denote the angular velocity and acceleration of the LVLH frame w.t.r. the ECI frame
\begin{equation*}
\boldsymbol{\omega} = \omega_{x} \hat{\mathbf{i}} + \omega_{y} \hat{\mathbf{j}} + \omega_{z} \hat{\mathbf{k}}  \qquad \dot{\boldsymbol{\omega}}= \dot{ \omega}_{x}  \hat{\mathbf{i}} + \dot{ \omega}_{y} \hat{\mathbf{j}} + \dot{\omega}_{z} \hat{\mathbf{k}}
\end{equation*}

where the dot operator indicates the derivation w.r.t. time in LVLH frame.\\ We can define the absolute position of chaser, expressed in LVLH frame, in this manner:
\begin{equation}
\mathbf{r}_{c} = \mathbf{r}_t + \boldsymbol{\rho} =  x \hat{\mathbf{i}} + y \hat{\mathbf{j}} + (z - r_t) \hat{\mathbf{k}}
\label{eq:absolute_position_chaser}
\end{equation} 

Chaser's absolute velocity can be obtained deriving w.r.t. time \cref{eq:absolute_position_chaser},
\begin{equation}
\frac{d}{dt}\mathbf{r}_{c} = \frac{d}{dt}\mathbf{r}_t + \frac{d}{dt}\boldsymbol{\rho} = \frac{d}{dt}\mathbf{r}_t + \dot{\boldsymbol{\rho}} + \boldsymbol{\omega} \times \boldsymbol{\rho} 
 \label{eq:absolute_velocity_chaser}
\end{equation}

Note that the derivation in the inertial frame is denoted with $\frac{d}{dt}$. \\
Moreover derivation of \cref{eq:absolute_velocity_chaser} leads to the chaser's vectorial equation of motion. Introducing chaser's and target's equations 
\begin{align}
\frac{d}{dt}\mathbf{r}_{c} &= -\frac{\mu}{{{r}_{c}}^{3}} \mathbf{r}_{c} + \mathbf{u}  \label{eq:keplerian_chaser_motion} \\ \frac{d}{dt}\mathbf{r}_t &= -\frac{\mu}{r_t^3} \mathbf{r}_t \label{eq:keplerian_target_motion}
\end{align} 

the general vectorial expression for the relative motion is given by
\begin{equation}
\ddot{\boldsymbol{\rho}} + 2 \boldsymbol{\omega} \times \dot{\boldsymbol{\rho}} + \dot{\boldsymbol{\omega}} \times \boldsymbol{\rho} + \boldsymbol{\omega} \times \left( \boldsymbol{\omega} \times \boldsymbol{\rho} \right) -\frac{\mu}{r_t^3} \mathbf{r}_t + \frac{\mu}{{{r}_{c}}^{3}} \left( \mathbf{r}_t + \boldsymbol{\rho} \right) = \mathbf{u}
\label{eq:relative_differential_vectorial}
\end{equation}

In absence of perturbation the angular velocity $\boldsymbol{\omega}$ becomes
\begin{equation*}
\boldsymbol{\omega} = \dot{f}\hat{\mathbf{j}} = \frac{h}{r_t^{2}}\hat{\mathbf{j}}
\end{equation*}

and the angular acceleration in Keplerian orbits is:

\begin{equation*}
\dot{\boldsymbol{\omega}} = -2\frac{h\dot{r}_t}{r_t^{3}}\hat{\mathbf{j}} = -2\dot{f}\frac{\dot{r}_t}{r_t}\hat{\mathbf{j}}
\end{equation*}

Now we have all necessary to write the \emph{Nonlinear Equation of Relative Motion} (NERM) along the LVLH unit vectors separately:

\begin{equation}
\begin{aligned}
\ddot{x} &= 2\dot{f} \left(\frac{\dot{r}_t}{r_t}z -\dot{z} \right) + \dot{f}^{2} x  - \frac{\mu}{{r_{c}}^{3}} x + u_{x} \\
\ddot{y} &=  - \frac{\mu}{{r_{c}}^{3}}y + u_{y} \\
\ddot{z} &= -2\dot{f} \left( \frac{\dot{r}_t}{r_t}x - \dot{x} \right) + \dot{f}^{2} z  + \frac{\mu}{r_t^{2}} - \frac{\mu}{{r_{c}}^{3}} \left (z -r_t \right ) + u_{z}
\end{aligned} \label{eq:NERM}
\end{equation}

Note that $r_t$, $\dot{r}_t$ and $\dot{f}$ are time - varing terms; $ \mathbf{x} = \begin{bmatrix} x \quad y \quad z \quad \dot{x} \quad \dot{y} \quad \dot{z} \end{bmatrix}^{T}$
\newpage
\subsection{Relative Motion in Three-Body Probelm} \label{sec:rel_mot_3BP}
Consider a target and a chaser spacecraft, orbiting around the second primaries, and subject to both first and second primaries gravitational influence. This section describe the motion of the chaser relative to the target, in a frame centered on the latter. To this end, the \emph{local-vertical local-horizon} (LVLH) frame $L: \begin{Bmatrix}\mathbf{R}_t; \hat{\mathbf{i}},\hat{\mathbf{i}}, \hat{\mathbf{i}} \end{Bmatrix}$ is introduced, with unit vectors defined as follow
\begin{equation}
\hat{\mathbf{i}} = \hat{\mathbf{j}} \times \hat{\mathbf{k}},\qquad \hat{\mathbf{j}} = -\frac{\mathbf{h}_{t/m}}{h_{t/m}}, \qquad \hat{\mathbf{k}} = -\frac{\mathbf{r}_{mt}}{r_{mt}}
\label{eq:LVLH3}
\end{equation}

where $\mathbf{r}_{mt}$ is the target position w.r.t. the Moon, $r_{mt} = ||\mathbf{r}_{mt}||$, $\mathbf{h}_{t/m} = \mathbf{r}_{mt} \times \begin{bmatrix} \dot{\mathbf{r}}_{mt} \end{bmatrix}_{\mathcal{M}}$ is the target angular momentum w.r.t. the Moon and $h_{t/m} = ||\mathbf{h}_{t/m}||$. The unit vectors $\hat{\mathbf{i}}, \hat{\mathbf{j}},\hat{\mathbf{k}}$ in the rendezvous literature are generally referred to as \emph{V-bar}, \emph{H-bar} and \emph{R-bar}, respectively \cite{Franzini2017}.
\begin{figure}[ht]
	\begin{minipage}[t]{7cm}
		\centering
    		\includegraphics[scale = 0.26]{relative_motion_moon}
    		\caption{\textit{Target and Chaser in Three-Body system.}}
    		\label{fig:rel_motion_moon}
	\end{minipage}
	\begin{minipage}[t]{7cm}
		\centering
    		\includegraphics[scale = 0.26]{LVLH_frame}
    		\caption{\textit{LVLH reference frame.}}
    		\label{fig:LVLH_frame}
	\end{minipage}
\end{figure}

The equation of relative motion in the LVLH frame was derived for the first time in \cite{Franzini2017} and are: 
\newline
\begin{equation} \begin{aligned} 
\begin{bmatrix} \ddot{\boldsymbol{\rho}}\end{bmatrix}_{\mathcal{L}} + 2\boldsymbol{\omega}_{l/i} \times \begin{bmatrix} \dot{\boldsymbol{\rho}}\end{bmatrix}_{\mathcal{L}} + \begin{bmatrix} \dot{\boldsymbol{\omega}}_{l/i}\end{bmatrix}_{\mathcal{L}} \times \boldsymbol{\rho} + \boldsymbol{\omega}_{l/i} \times \boldsymbol{\omega}_{l/i} \times \boldsymbol{\rho} =\\  = \mu_m \left (\frac{\mathbf{r}_{mt}}{r_{mt}^3} - \frac{\mathbf{r}_{mc}}{r_{mc}^3} \right) + \mu_e \left (\frac{\mathbf{r}_{et}}{r_{et}^3} - \frac{\mathbf{r}_{ec}}{r_{ec}^3} \right)
\end{aligned} \label{eq_rel_motion_moon}
\end{equation}
\newline
where, as you can see in \cref{fig:rel_motion_moon}, 
\begin{equation*}
\mathbf{r}_{mc} = \mathbf{r}_{mt} + \boldsymbol{\rho},\quad \mathbf{r}_{et} = \mathbf{r}_{mt} + \mathbf{r}_{em}, \quad \mathbf{r}_{ec} = \mathbf{r}_{et} + \boldsymbol{\rho}
\end{equation*}

The second primary is not negligible in this case, a second gravitational term is introduced.
\end{document}
