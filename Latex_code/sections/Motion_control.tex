\documentclass[../main.tex]{thesis_style}
\graphicspath{{images/}{../images/}}

\begin{document}
\part{Motion Control}
\newpage
\section{SDRE Nonlinear Control} \label{sec:control}
The goal of this work is to find a controller that permit a rendezvous between the target and the chaser using the SDRE technique,  an extension of LQR theory on nonlinear problems known as \emph{State-Dependent Riccati Equation (SDRE)} nonlinear regulation \cite{Cimen2012}.The SDRE strategy provides an effective algorithm for synthesizing nonlinear feedback controls by allowing nonlinearities in the system states.

\subsection{Problem Formulation}
Consider a nonlinear dynamic system affine in the control:
\begin{equation}
\dot{\textbf{x}} = \mathbf{f(x)} + \mathbf{g(x)} \mathbf{u}
\label{eq:non_lin_dyn_sys}
\end{equation}

where $\mathbf{x} \in \mathbb{R}^{n}$ is the state vector, $ \mathbf{u} \in \mathbb{R}^{m}$ is the input vector, $\mathbf{f}: \mathbb{R}^{n} \to \mathbb{R}^{n} $ and $\mathbf{g(x)} \ne \boldsymbol{0}$  $\forall \mathbf{x} \in \mathbb{R}^{n}$ . As the performance index to be minimized, the following infinite-time integral is considered:
\begin{equation}
 J(\textbf{x}, \textbf{u}) = \frac{1}{2}\int_{0}^{\infty}\left ( \textbf{x}^{T}\textbf{Q}(\textbf{x})\textbf{x} + \textbf{u}^{T}\textbf{R}(\textbf{x})\textbf{u}\right ) \,dt
\end{equation}

where $\mathbf{Q}(\mathbf{x}) \ge 0$ and $\mathbf{R}(\mathbf{x}) > 0$  are weighing matrices of the state and the control vectors respectively. To solve the optimal control problem using SDRE method. If the nonlinear dynamic equation \cref{eq:non_lin_dyn_sys} can be written into a pseudo-linear structure using \emph{State Dependent Coefficient} (SDC) parameterization, like this:
\begin{equation}
\dot{\mathbf{x}} = \mathbf{A(x)} \mathbf{x} + \mathbf{B(x)} \mathbf{u}
\label{eq:sdc_dyn_sys}
\end{equation}

the SDRE technique can be applied. However, there are some requirements on the SDC parametrization to guarantee that \cref{eq:sdc_dyn_sys} has a solution: the pair $\begin{Bmatrix}\mathbf{A}(\mathbf{x}) ;\mathbf{B}(\mathbf{x})\end{Bmatrix}$ must be pointwise controllable in the linear sense.\\ The control technique consists of the following two phases for each time step:

\begin{enumerate}
\item Solve the state-dependent Riccati equation:
\begin{equation}
   \mathbf{A}^{T}(\mathbf{x})\mathbf{P}(\mathbf{x}) + \mathbf{P}(\mathbf{x})\mathbf{A}(\mathbf{x}) - \mathbf{P}(\mathbf{x})\mathbf{B}(\mathbf{x})\mathbf{R}^{-1}(\mathbf{x})\mathbf{B}^{T}(\mathbf{x})\mathbf{P}(\mathbf{x}) + \mathbf{Q}(\mathbf{x})
\end{equation}
\item Derive the feedback controller
\begin{equation}
 \mathbf{u} = -\mathbf{R}^{-1}(\mathbf{x})\mathbf{B}^{T}(\mathbf{x})\mathbf{P}(\mathbf{x})\mathbf{x}
\end{equation}
\end{enumerate}
\newpage

\subsection{SDC Parametrization}
SDC parametrization is a mathematical factorization of nonlinear system into a linearlike structure. $$\mathbf{f(x)} = \mathbf{A(x)x}$$
The folowing statements, under conditions expressed in \cite{Cimen2012}, garantee the parametrization existence and some linear system property:
\begin{itemize}
\item Let $\mathbf{f}: \Omega \to \mathbb{R}^{n}$ be such that $\mathbf{f(0)} = \boldsymbol{0}$ and $\mathbf{f(\cdot)} \in C^{k}(\Omega)$, $ k>1$. Then, for all $\mathbf{x}\in \Omega$, a SDC parametrization for $\mathbf{f(x)}$ always exists for $C^{k-1}$ matrix-valued function $\mathbf{A}: \Omega \to \mathbb{R}^{n \times n}$. \\
\item The SDC parametrization is a stabilizable (or controllable) parametrization of a nonlinear system [\cref{eq:sdc_dyn_sys}] in region $\Omega$ if the pair $\{\mathbf{A(x),B(x)}\}$ is pointwise stabilizable (or controllable) in the linear sense for all $\mathbf{x} \in \Omega$. \\
  \item The SDC parametrization is a detectable (or observable) parametrization of a nonlinear system [\cref{eq:sdc_dyn_sys}] in region $\Omega$ if the pair $\{\mathbf{C(x),A(x)}\}$ is pointwise detectable (or observable) in the linear sense for all $\mathbf{x} \in \Omega$.
\end{itemize}

Note that, in multivariable case, the parametrization is not unique. This allows to achieve better performance or to satisfy some project's requirement.
 
\subsubsection{Parametrization in Two-Body Problem Case}\label{subsubsec:param_two_body_prob}

Nonlinear equation of relative motion can be written in affine form like \cref{eq:non_lin_dyn_sys}, as follow:
\begin{equation}
	\mathbf{A(x)} = \begin{bmatrix}
    			0 & 0 & 0 & 1 & 0 & 0 \\ 0 & 0 & 0 & 0 & 1 & 0 \\ 0 & 0 & 0 & 0 & 0 & 1 \\
    			\dot{f}^{2}  - \frac{\mu}{{r_{c}}^{3}} & 0 & 2\dot{f}\frac{\dot{r}_t}{r_t}  & 0 & 0 &  2\dot{f}\\
			0 & - \frac{\mu}{{r_{c}}^{3}} & 0 & 0 & 0 & 0\\
			-2\dot{f}\frac{\dot{r}_t}{r_t} +\gamma x & \gamma y &  \dot{f}^{2} & -2\dot{f}  +\gamma ( z - r_t) - \frac{\mu}{r_{c}^3} & 0 & 0
  				\end{bmatrix}, \qquad 
	\mathbf{B(x)} = \begin{bmatrix}
    			\mathbf{0}_{3 \times 3} \\
    			\mathbf{I}_{3 \times 3}
  				\end{bmatrix} \label{eq:SDC_NERM_2BP}
\end{equation}
\newline
The Earth nonlinear gravitational term can be written in linearlike structure as follow: \newline
\begin{equation}
\mu \left ( \frac{\mathbf{r}_{t}}{r_{t}^3} - \frac{\mathbf{r}_{c}}{r_{c}^3} \right ) = \begin{bmatrix} -\frac{\mu}{r_{c}^3} &0 &0 \\
               0& -\frac{\mu}{r_{c}^3} &0 \\
             \gamma x & \gamma y & \gamma (z - r_{t})  - \frac{\mu}{r_{c}^3} \end{bmatrix} \boldsymbol{\rho} = \mathbf{A}_g\mathbf{(x)} \boldsymbol{\rho}
\end{equation}

where $\mathbf{r}_c = \mathbf{r}_t + \boldsymbol{\rho}$, $\boldsymbol{\rho} = \begin{bmatrix} x &y &z \end{bmatrix}^T$, $ f $ is the \emph{target true anomalie}, and 
\begin{equation*}
	 \gamma = -\mu \frac{(r_{c}^2 + r_{t}r_{c} + r_{t}^2)}{(r_{c} + r_{t})(r_{c}^3 r_{t}^2)}
\end{equation*}

This SDC representation make system controllable. For more details see \cite{Franzini2014}. 

\subsubsection{SDC Parametrization for Relative Motion around Moon}\label{subsubsec:param_three_body_prob}
Nonlinear equation of relative motion, \cref{eq_rel_motion_moon} can be easily rewritten in affine form like \cref{eq:non_lin_dyn_sys} and all conditions expressed in  \cite{Cimen2012} are met, so a possible SDC parametrization exists. Note that the nonlinearities are concentrated in gravitational terms. \\
The Moon gravitational attraction term can be written as: \newline
\begin{equation}
\mu_m \left ( \frac{\mathbf{r}_{mt}}{r_{mt}^3} - \frac{\mathbf{r}_{mc}}{r_{mc}^3} \right ) = \begin{bmatrix} -\frac{\mu_m}{r_{mc}^3} &0 &0 \\
               0& -\frac{\mu_m}{r_{mc}^3} &0 \\
             \gamma_m x & \gamma_m y & \gamma_m (z - r_{mt})  - \frac{\mu_m}{r_{mc}^3} \end{bmatrix} \boldsymbol{\rho} = \mathbf{A}_m\mathbf{(x)} \boldsymbol{\rho}
\end{equation}
\newline
The Earth gravitational attraction term can be written as: 
\begin{multline}
\mu_e \left ( \frac{\mathbf{r}_{mt} + \mathbf{r}_{em}}{|| \mathbf{r}_{mt} + \mathbf{r}_{em} ||^3} - \frac{\mathbf{r}_{mt} + \mathbf{r}_{em} +\boldsymbol{\rho}}{|| \mathbf{r}_{mt} + \mathbf{r}_{em} +\boldsymbol{\rho} ||^3} \right ) = \quad \mathbf{A}_e\mathbf{(x)} \boldsymbol{\rho} =  \\ \\ 
\begin{bmatrix} \gamma_e r_{em}^{x} (2 r_{em}^x + x) - \frac{\mu_e}{r_{ec}^3} & \gamma_e r_{em}^x (2 r_{em}^y + y) & \gamma_e r_{em}^x (2 (r_{em}^z - r_{mt}) + z)\\
          \gamma_e r_{em}^y (2 r_{em}^x + x) & \gamma_e r_{em}^y (2 r_{em}^y + y)  - \frac{\mu_e}{r_{ec}^3} & \gamma_e r_{em}^y (2 (r_{em}^z - r_{mt}) + z)\\
         \gamma_e (r_{em}^z - r_{mt}) (2 r_{em}^x + x) & \gamma_e (r_{em}^z - r_{mt}) (2 r_{em}^y + y) & \gamma_e (r_{em}^z - r_{mt}) (2 (r_{em}^z - r_{mt}) + z) - \frac{\mu_e}{r_{ec}^3} \end{bmatrix} \boldsymbol{\rho}
\end{multline}
\newline
where \\ $\boldsymbol{\rho} =  \begin{bmatrix} x &y &z \end{bmatrix}^T$, $\mathbf{r}_{mt} =  \begin{bmatrix} 0& 0& -r_{mt} \end{bmatrix}^T$, $\mathbf{r}_{mc} =  \begin{bmatrix} x &y& z-r_{mt} \end{bmatrix}^T$, $\mathbf{r}_{em} =  \begin{bmatrix} r_{em}^x &r_{em}^y & r_{em}^z \end{bmatrix}^T$, $\mathbf{r}_{ec} = \mathbf{r}_{em} + \mathbf{r}_{mt} + \boldsymbol{\rho}$ and

\begin{equation*}
  \begin{aligned}
  \gamma_m = -\mu_m \frac{(r_{mc}^2 + r_{mt}r_{mc} + r_{mt}^2)}{(r_{mc} + r_{mt})(r_{mc}^3 r_{mt}^2)}, \quad \gamma_e = \mu_e\frac{(r_{ec}^2 + r_{et}r_{ec} + r_{et}^2)}{(r_{ec} + r_{et})(r_{ec}^3 r_{et}^3)}
  \end{aligned}
\end{equation*}
\newline
The SDC parametrization of relative motion, \cref{eq_rel_motion_moon}, now can be written as follow:
\begin{equation}
\mathbf{A(x)} = \begin{bmatrix} \boldsymbol{0}_{3 \times 3} & \mathbf{I}_{3 \times 3} \\ -\begin{bmatrix}\dot{\Omega}_{l/i}\end{bmatrix}_\mathcal{L} - \Omega_{l/i}^2 + \mathbf{A}_m\mathbf{(x)} + \mathbf{A}_e\mathbf{(x)} & -2 \Omega_{l/i} 
\end{bmatrix}, \quad \mathbf{B(x)} = \begin{bmatrix} \boldsymbol{0}_{3 \times 3} \\ \mathbf{I}_{3 \times 3} \end{bmatrix} 
\label{eq:SDC_NERM_moon} 
\end{equation}
\newline
$\mathbf{x} = \begin{bmatrix} \boldsymbol{\rho} & \dot{\boldsymbol{\rho}} \end{bmatrix}^T$ is the state of dynamical system, $\begin{bmatrix}\dot{\Omega}_{l/i}\end{bmatrix}_\mathcal{L}$ and $\Omega_{l/i}$ denote the skew-symmetric matrix associated to $\begin{bmatrix}\dot{\omega}_{l/i}\end{bmatrix}_\mathcal{L}$ and $\omega_{l/i}$, respectively. 

\newpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Constraints Formulation}
The rendezvous maneuver can be faced imposing constraint on relative spacecraft position and velocity while approaching the docking port on a target platform.\\Considering an input-affine system as \cref{eq:non_lin_dyn_sys}, with $\mathbf{x(0)} = \mathbf{x}_{0} \in \Omega$ and the set of allowable states defined by
\begin{equation}
 \Omega = \begin{Bmatrix} \mathbf{x}:\mathbf{l(x)} \le \boldsymbol{0}, \mathbf{l(x)} \in \mathbb{R}^{p}, \mathbf{l(\cdot)} \in C^{1} \end{Bmatrix}
\end{equation}
it is possible to design a state feedback controller such that the closed-loop system is stable and $\mathbf{x}$ does not cross $\partial \Omega$, the boudary of $\Omega$, defined as:
\begin{equation}
 \partial \Omega = \begin{Bmatrix} \mathbf{x}:\mathbf{l(x)} = \boldsymbol{0}, \mathbf{l(x)} \in \mathbb{R}^{p}, \mathbf{l(\cdot)} \in C^{1} \end{Bmatrix}
\end{equation}

The sufficient condition for $\mathbf{x}$ to remain in $\Omega$ is $\dot{\mathbf{l}}\mathbf{(x)} = \boldsymbol{0}$:
\begin{equation}
\nabla \mathbf{l(x)}\dot{\mathbf{x}} = \nabla \mathbf{l(x)} \begin{bmatrix}\mathbf{f(x)} + \mathbf{g(x)} \mathbf{u} \end{bmatrix} = \boldsymbol{0}
\label{eq:cond_constraint}
\end{equation}

It is possible to express the condition above in SDC form and add a fictitious output $\mathbf{z}$ to SDRE problem
\begin{equation}
\begin{split}
\mathbf{z} & = \nabla \mathbf{l(x)} \begin{bmatrix} \mathbf{A(x)}\mathbf{x} + \mathbf{B(x)} \mathbf{u} \end{bmatrix}\\
 & = \mathbf{C(x)}\mathbf{x} + \mathbf{D(x)} \mathbf{u}
\end{split}
\end{equation}

A controller that satisfies the condition on \cref{eq:cond_constraint} forces the closed-loop trajectories to follow the level set of $\Omega$.\\ So we need an augmented cost function:
\begin{equation}
\begin{split}
 J(\mathbf{x}, \mathbf{u}) & = J_{0}(\mathbf{x}, \mathbf{u}) + J_{\Omega}(\mathbf{x}, \mathbf{u}) = \\
& = \frac{1}{2}\int_{0}^{\infty}\left ( \textbf{x}^{T}\textbf{Q}(\textbf{x})\textbf{x} + \textbf{u}^{T}\textbf{R}(\textbf{x})\textbf{u}\right ) \,dt + \frac{1}{2}\int_{0}^{\infty}\left ( \mathbf{z}^{T}\mathbf{W}_{z}(\mathbf{x})\mathbf{z}\right ) \,dt
\end{split}\label{eq:new_cost_fuction}
\end{equation}

where $\mathbf{W}_z$ is a $p \times p$ diagonal matrix, such that its \emph{i-}th element is large when $\mathbf{x}$ is close to the boundary of \emph{i}th constraint and large otherwise. It increases $J_{\Omega}(\mathbf{x}, \mathbf{u})$  far above the performance index $J_{0}(\mathbf{x}, \mathbf{u})$ when the chaser hold not constraints.\\ In this case the SDRE problem become: 
\begin{equation}
	\begin{gathered}
\bar{\mathbf{A}}^{T}(\mathbf{x})\bar{\mathbf{P}}(\mathbf{x}) + \bar{\mathbf{P}}(\mathbf{x})\bar{\mathbf{A}}(\mathbf{x}) - \bar{\mathbf{P}}(\mathbf{x})\bar{\mathbf{B}}(\mathbf{x})\bar{\mathbf{R}}^{-1}(\mathbf{x})\bar{\mathbf{B}}^{T}(\mathbf{x})\bar{\mathbf{P}}(\mathbf{x}) + \bar{\mathbf{Q}}(\mathbf{x}) = 0,\\ \\
\bar{\mathbf{R}}(\mathbf{x}) = \mathbf{R}(\mathbf{x}) + \mathbf{D}^{T}(\mathbf{x})\mathbf{W}_{z}(\mathbf{x})\mathbf{D}(\mathbf{x}),\\ \\
\bar{\mathbf{Q}}(\mathbf{x}) = \mathbf{Q}(\mathbf{x}) + \mathbf{C}^{T}(\mathbf{x})\mathbf{W}_{z}(\mathbf{x})\begin{bmatrix}\mathbf{I} - \mathbf{D}^{T}(\mathbf{x})\bar{\mathbf{R}}^{-1}(\mathbf{x})\mathbf{D}(\mathbf{x})\end{bmatrix}\mathbf{W}_{z}(\mathbf{x})\mathbf{C}(\mathbf{x}) \\ \\
\bar{\mathbf{A}}(\mathbf{x}) = \mathbf{A}(\mathbf{x}) - \mathbf{B}(\mathbf{x})\bar{\mathbf{R}}^{-1}(\mathbf{x})\mathbf{D}^{T}(\mathbf{x})\mathbf{W}_{z}(\mathbf{x})\mathbf{C}(\mathbf{x}).
	\end{gathered}\label{eq:new_sdre}
\end{equation}

The resulting control law is composed:
\begin{equation}
\begin{split}
\mathbf{u} &= - \mathbf{K}(\mathbf{x})\mathbf{x} \\
&= - \begin{bmatrix}\mathbf{K}_{0}(\mathbf{x}) + \mathbf{K}_{\Omega}(\mathbf{x})\end{bmatrix}\mathbf{x}
\end{split}\label{eq:control_gain}
\end{equation}

where:
\begin{align}
	\begin{gathered}
\mathbf{K}_{0}(\mathbf{x}) = \bar{\mathbf{R}}^{-1}(\mathbf{x})\mathbf{B}(\mathbf{x})\bar{\mathbf{P}}(\mathbf{x}) \\
\mathbf{K}_{\Omega}(\mathbf{x}) = \bar{\mathbf{R}}^{-1}(\mathbf{x})\mathbf{D}^{T}(\mathbf{x})\mathbf{W}_{z}(\mathbf{x})\mathbf{C}(\mathbf{x})
	\end{gathered}
\end{align}

$\mathbf{K}_{0}(\mathbf{x})$ is used for stabilization/performance and $\mathbf{K}_{\Omega}(\mathbf{x})$ is used to satisfy the constraints \cite{Massari2012}.
%%%%%%%%%%%%%%%%
\subsubsection{Constraint design} \label{sec:weight_function}
The constraints of LOS require the chaser to remain within the LOS-cone. The approaching cone is mathematically defined by:
\begin{equation*}
	\begin{matrix}
		\begin{cases} \begin{aligned}
			-\sin(\beta) \cdot x + \cos(\beta) \cdot z < 0 \\
			-\sin(\beta) \cdot x - \cos(\beta) \cdot z < 0 \\
			x < 0 \end{aligned}
		\end{cases} \\ \\
  		\begin{cases} \begin{matrix}
    			a(x,z)\textbf{:} &\quad &-\sin(\beta) \cdot x + \cos(\beta) \cdot z = 0 \\
    			b(x,z)\textbf{:} &\quad &-\sin(\beta) \cdot x - \cos(\beta) \cdot z = 0 \\
			c(x,z)\textbf{:} &\quad & z = 0 \end{matrix}
  		\end{cases}
 	\end{matrix} \label{eq:LOS_cone_constraint}
\end{equation*}
\newline
\begin{figure}[h]
	\centering
	\includegraphics[scale=0.3]{LineOfSightCone} 
    	\caption{\textit{Approaching cone for docking port on a target platform.}}
    	\label{fig:LOS_cone}
\end{figure}

The ${\mathbf{W}}_{z}$ weight function is similar to \emph{Artificial Potential Function} (APF) that describes the attractive area (\cref{fig:axis_weight_function}) and the avoidance region (\cref{fig:cone_weight_function}). A suitable choice can be to create a shell around the target with a preferential region, like LOS cone, on the docking port. To this it is preferable to add another function that encourage the chaser to come following the cone axis.
\begin{figure}[h]
	\centering
	\includegraphics[scale = 0.225]{axis_weight_function}
	\caption{\textit{Weight functions on thirth fictious output}, $f_{2}$.}
	\label{fig:axis_weight_function}
\end{figure}
\vspace{3mm}
\begin{figure}[h]
	\centering
	\includegraphics[scale = 0.225]{cone_weight_function}
	\caption{\textit{Weight functions on first and second fictious outputs}, $f_{1}$.}
	\label{fig:cone_weight_function}
\end{figure}

The mathematical expression of functions are: 
\begin{equation}
\begin{matrix}
  f_{cone}(x,z) = [sign(a(x,z)) + sign(b(x,z)) + 2] \cdot S(\sqrt{x^2 + z^2}) \cdot S(r_{w} - \sqrt{x^2 + z^2})\\ \\
  f_{axis}(x,z) = \frac{\begin{vmatrix}  z - m x  \end{vmatrix}}{\sqrt{1 + m^2}} 
  \end{matrix}
   \label{eq:weight_functions}
\end{equation}

where $S(\cdot)$ is logistic function, $sign(\cdot)$ is the signum function, $m$ is the slope of line and $r_{w}$ is the blast radius of LOS cone.\\ However the constraints are softly imposed by the minimization of cost function, so it not guarantee that the states reach the desired values if the weight matrices are not suitable.

\newpage
\end{document}
