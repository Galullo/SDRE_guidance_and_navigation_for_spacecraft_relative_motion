import geometry; 
unitsize(1cm);

// define the xy-axis
draw((-4,0)--(4,0), arrow = Arrow); label("$R-bar$",(-2.8,0.5), p = fontsize(10pt),S);
draw((0,-4)--(0,4), arrow = Arrow); label("$V-bar$",(-0.1,-3.8), p = fontsize(10pt),W);

// define line of costraint
//draw((-2*sin(pi/6), -2*cos(pi/6))--(2*sin(pi/6), 2*cos(pi/6)), red, arrow=Arrow(TeXHead)); label("$a$", (1.4,1.2), p = fontsize(10pt),NW);
//draw((2*sin(pi/6), -2*cos(pi/6))--(-2*sin(pi/6), 2*cos(pi/6)), red, arrow=Arrow(TeXHead)); label("$b$", (1.4,-1.2), p = fontsize(10pt),SW);

draw((2*sin(pi/6), 2*cos(pi/6))--(-2*sin(pi/6), -2*cos(pi/6)), red, arrow=Arrow(TeXHead)); label("$a$", (1.4,1.2), p = fontsize(10pt),NW);
draw((-2*sin(pi/6), 2*cos(pi/6))--(2*sin(pi/6), -2*cos(pi/6)), red, arrow=Arrow(TeXHead)); label("$b$", (1.4,-1.2), p = fontsize(10pt),SW);

// angle of LOS-cone
draw(arc((0,0), r=1, angle1=-90, angle2=30-90), blue, arrow=Arrow(TeXHead)); label("-$\gamma$",(0.7*sin(pi/12-pi/2),-2.5*cos(pi/6-pi/2)),E);
draw(arc((0,0), r=1.1, angle1=-90, angle2=-30-90), blue, arrow=Arrow(TeXHead)); label("$\gamma$",(-0.1*sin(pi/12-pi/2),-2.5*cos(pi/6-pi/2)),E);