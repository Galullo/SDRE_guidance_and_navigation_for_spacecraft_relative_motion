FOLDERS

- Matlab code	: contiene tutto il codice Matlab;
- slide	 	: contiene il codice Latex per la presentazione di pre-laurea;
- Thesis	: contiene il codice Latex della tesi;

per il codice Latex basta lanciare il comando "make" da terminale e compila tutto il codice. Come uscita si ha il file "main.pdf"
